#ifndef CW_BASE_H
#define CW_BASE_H

#include <stdlib.h>

typedef char CW_CHAR;

typedef char CW_S8;

typedef unsigned char CW_U8;

typedef short CW_S16;

typedef unsigned short CW_U16;

typedef int CW_S32;

typedef unsigned int CW_U32;

typedef long CW_S64;

typedef unsigned long CW_U64;

typedef float CW_R32;

typedef double CW_R64;

typedef void CW_VOID;

typedef size_t CW_SIZE;

typedef enum cw_bool_t
{
  CW_BOOL_FALSE = 0,

  CW_BOOL_TRUE = 1
} CW_BOOL;

#endif /* CW_BASE_H */
