#ifndef CW_MATH_H
#define CW_MATH_H

#include "cw_base.h"

CW_BOOL cw_math_compare_r32 (const CW_R32 first, const CW_R32 second);

CW_BOOL cw_math_compare_r64 (const CW_R64 first, const CW_R64 second);

#endif /* CW_MATH_H */
