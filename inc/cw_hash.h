#ifndef CW_HASH_H
#define CW_HASH_H

#include "cw_base.h"

CW_BOOL cw_hash_fnv1 (CW_U64 *const hash, const CW_U8 *const data,
                      const CW_SIZE length);

CW_BOOL cw_hash_fnv1_string (CW_U64 *const hash, const CW_VOID *const data);

CW_BOOL cw_hash_integer (CW_U64 *const hash, const CW_VOID *const data);

#endif /* CW_HASH_H */
