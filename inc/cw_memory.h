#ifndef CW_MEMORY_H
#define CW_MEMORY_H

#include "cw_base.h"

#include <stdio.h>

/* Uncomment the following line to use standard malloc/free. */

/* #define CW_MEMORY_UNSAFE */

CW_BOOL cw_memory_create (CW_VOID);

CW_BOOL cw_memory_delete (CW_VOID);

CW_BOOL cw_memory_clear (CW_VOID);

CW_VOID *cw_memory_allocate (const CW_SIZE size);

CW_VOID *cw_memory_reallocate (CW_VOID *const memory, const CW_SIZE size);

CW_BOOL cw_memory_free (CW_VOID **const memory);

CW_BOOL cw_memory_is_allocated (const CW_VOID *const memory);

CW_BOOL cw_memory_dump (FILE *const file);

#endif /* CW_MEMORY_H */
