#ifndef CW_DUPLICATE_H
#define CW_DUPLICATE_H

#include "cw_base.h"

CW_CHAR *cw_duplicate_char (const CW_CHAR value);

CW_S8 *cw_duplicate_s8 (const CW_S8 value);

CW_U8 *cw_duplicate_u8 (const CW_U8 value);

CW_S16 *cw_duplicate_s16 (const CW_S16 value);

CW_U16 *cw_duplicate_u16 (const CW_U16 value);

CW_S32 *cw_duplicate_s32 (const CW_S32 value);

CW_U32 *cw_duplicate_u32 (const CW_U32 value);

CW_S64 *cw_duplicate_s64 (const CW_S64 value);

CW_U64 *cw_duplicate_u64 (const CW_U64 value);

CW_SIZE *cw_duplicate_size (const CW_SIZE value);

CW_BOOL *cw_duplicate_bool (const CW_BOOL value);

CW_CHAR *cw_duplicate_string (const CW_CHAR *const string);

#endif /* CW_DUPLICATE_H */
