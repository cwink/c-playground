#ifndef CW_ERROR_H
#define CW_ERROR_H

#include "cw_base.h"

#ifndef CW_ERROR_MAX_MESSAGE_LENGTH
#define CW_ERROR_MAX_MESSAGE_LENGTH 4096
#endif /* CW_ERROR_MAX_MESSAGE_LENGTH */

#define CW_ERROR_WRITE(FORMAT) cw_error_write (__FILE__, __LINE__, FORMAT)

#define CW_ERROR_WRITE_1(FORMAT, A)                                           \
  cw_error_write (__FILE__, __LINE__, FORMAT, A)

#define CW_ERROR_WRITE_2(FORMAT, A, B)                                        \
  cw_error_write (__FILE__, __LINE__, FORMAT, A, B)

#define CW_ERROR_WRITE_3(FORMAT, A, B, C)                                     \
  cw_error_write (__FILE__, __LINE__, FORMAT, A, B, C)

#define CW_ERROR_WRITE_4(FORMAT, A, B, C, D)                                  \
  cw_error_write (__FILE__, __LINE__, FORMAT, A, B, C, D)

#define CW_ERROR_WRITE_5(FORMAT, A, B, C, D, E)                               \
  cw_error_write (__FILE__, __LINE__, FORMAT, A, B, C, D, E)

#define CW_ERROR_WRITE_6(FORMAT, A, B, C, D, E, F)                            \
  cw_error_write (__FILE__, __LINE__, FORMAT, A, B, C, D, E, F)

#define CW_ERROR_WRITE_7(FORMAT, A, B, C, D, E, F, G)                         \
  cw_error_write (__FILE__, __LINE__, FORMAT, A, B, C, D, E, F, G)

#define CW_ERROR_WRITE_8(FORMAT, A, B, C, D, E, F, G, H)                      \
  cw_error_write (__FILE__, __LINE__, FORMAT, A, B, C, D, E, F, G, H)

#define CW_ERROR_CHECK(CONDITION, RETURN, FORMAT)                             \
  do                                                                          \
    {                                                                         \
      if ((CONDITION) == 0)                                                   \
        {                                                                     \
          CW_ERROR_WRITE (FORMAT);                                            \
                                                                              \
          return RETURN;                                                      \
        }                                                                     \
    }                                                                         \
  while (0)

#define CW_ERROR_CHECK_1(CONDITION, RETURN, FORMAT, A)                        \
  do                                                                          \
    {                                                                         \
      if ((CONDITION) == 0)                                                   \
        {                                                                     \
          CW_ERROR_WRITE_1 (FORMAT, A);                                       \
                                                                              \
          return RETURN;                                                      \
        }                                                                     \
    }                                                                         \
  while (0)

#define CW_ERROR_CHECK_2(CONDITION, RETURN, FORMAT, A, B)                     \
  do                                                                          \
    {                                                                         \
      if ((CONDITION) == 0)                                                   \
        {                                                                     \
          CW_ERROR_WRITE_2 (FORMAT, A, B);                                    \
                                                                              \
          return RETURN;                                                      \
        }                                                                     \
    }                                                                         \
  while (0)

#define CW_ERROR_CHECK_3(CONDITION, RETURN, FORMAT, A, B, C)                  \
  do                                                                          \
    {                                                                         \
      if ((CONDITION) == 0)                                                   \
        {                                                                     \
          CW_ERROR_WRITE_3 (FORMAT, A, B, C);                                 \
                                                                              \
          return RETURN;                                                      \
        }                                                                     \
    }                                                                         \
  while (0)

#define CW_ERROR_CHECK_4(CONDITION, RETURN, FORMAT, A, B, C, D)               \
  do                                                                          \
    {                                                                         \
      if ((CONDITION) == 0)                                                   \
        {                                                                     \
          CW_ERROR_WRITE_4 (FORMAT, A, B, C, D);                              \
                                                                              \
          return RETURN;                                                      \
        }                                                                     \
    }                                                                         \
  while (0)

#define CW_ERROR_CHECK_5(CONDITION, RETURN, FORMAT, A, B, C, D, E)            \
  do                                                                          \
    {                                                                         \
      if ((CONDITION) == 0)                                                   \
        {                                                                     \
          CW_ERROR_WRITE_5 (FORMAT, A, B, C, D, E);                           \
                                                                              \
          return RETURN;                                                      \
        }                                                                     \
    }                                                                         \
  while (0)

#define CW_ERROR_CHECK_6(CONDITION, RETURN, FORMAT, A, B, C, D, E, F)         \
  do                                                                          \
    {                                                                         \
      if ((CONDITION) == 0)                                                   \
        {                                                                     \
          CW_ERROR_WRITE_6 (FORMAT, A, B, C, D, E, F);                        \
                                                                              \
          return RETURN;                                                      \
        }                                                                     \
    }                                                                         \
  while (0)

#define CW_ERROR_CHECK_7(CONDITION, RETURN, FORMAT, A, B, C, D, E, F, G)      \
  do                                                                          \
    {                                                                         \
      if ((CONDITION) == 0)                                                   \
        {                                                                     \
          CW_ERROR_WRITE_7 (FORMAT, A, B, C, D, E, F, G);                     \
                                                                              \
          return RETURN;                                                      \
        }                                                                     \
    }                                                                         \
  while (0)

#define CW_ERROR_CHECK_8(CONDITION, RETURN, FORMAT, A, B, C, D, E, F, G, H)   \
  do                                                                          \
    {                                                                         \
      if ((CONDITION) == 0)                                                   \
        {                                                                     \
          CW_ERROR_WRITE_8 (FORMAT, A, B, C, D, E, F, G, H);                  \
                                                                              \
          return RETURN;                                                      \
        }                                                                     \
    }                                                                         \
  while (0)

typedef CW_VOID (*CW_ERROR_CALLBACK) (const CW_CHAR *const filename,
                                      const CW_SIZE line,
                                      const CW_CHAR *const message);

CW_VOID cw_error_callback_set (const CW_ERROR_CALLBACK callback);

CW_VOID cw_error_write (const CW_CHAR *const filename, const CW_SIZE line,
                        const CW_CHAR *const format, ...);

#endif /* CW_ERROR_H */
