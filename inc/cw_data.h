#ifndef CW_DATA_H
#define CW_DATA_H

#include "cw_base.h"

typedef CW_VOID *(*CW_DATA_COPY) (const CW_VOID *const value);

typedef CW_BOOL (*CW_DATA_DELETE) (CW_VOID *value);

typedef CW_BOOL (*CW_DATA_COMPARE) (const CW_VOID *const left,
                                    const CW_VOID *const right);

typedef CW_BOOL (*CW_DATA_FOREACH) (CW_VOID *const value);

typedef CW_BOOL (*CW_DATA_FOREACH_PAIR) (const CW_VOID *const key,
                                         CW_VOID *const value);

typedef CW_BOOL (*CW_DATA_HASHER) (CW_U64 *const hash,
                                   const CW_VOID *const data);

CW_VOID *cw_data_func_char_copy (const CW_VOID *const value);

CW_BOOL cw_data_func_char_delete (CW_VOID *value);

CW_BOOL cw_data_func_char_compare (const CW_VOID *const left,
                                   const CW_VOID *const right);

CW_VOID *cw_data_func_s8_copy (const CW_VOID *const value);

CW_BOOL cw_data_func_s8_delete (CW_VOID *value);

CW_BOOL cw_data_func_s8_compare (const CW_VOID *const left,
                                 const CW_VOID *const right);

CW_VOID *cw_data_func_u8_copy (const CW_VOID *const value);

CW_BOOL cw_data_func_u8_delete (CW_VOID *value);

CW_BOOL cw_data_func_u8_compare (const CW_VOID *const left,
                                 const CW_VOID *const right);

CW_VOID *cw_data_func_s16_copy (const CW_VOID *const value);

CW_BOOL cw_data_func_s16_delete (CW_VOID *value);

CW_BOOL cw_data_func_s16_compare (const CW_VOID *const left,
                                  const CW_VOID *const right);

CW_VOID *cw_data_func_u16_copy (const CW_VOID *const value);

CW_BOOL cw_data_func_u16_delete (CW_VOID *value);

CW_BOOL cw_data_func_u16_compare (const CW_VOID *const left,
                                  const CW_VOID *const right);

CW_VOID *cw_data_func_s32_copy (const CW_VOID *const value);

CW_BOOL cw_data_func_s32_delete (CW_VOID *value);

CW_BOOL cw_data_func_s32_compare (const CW_VOID *const left,
                                  const CW_VOID *const right);

CW_VOID *cw_data_func_u32_copy (const CW_VOID *const value);

CW_BOOL cw_data_func_u32_delete (CW_VOID *value);

CW_BOOL cw_data_func_u32_compare (const CW_VOID *const left,
                                  const CW_VOID *const right);

CW_VOID *cw_data_func_s64_copy (const CW_VOID *const value);

CW_BOOL cw_data_func_s64_delete (CW_VOID *value);

CW_BOOL cw_data_func_s64_compare (const CW_VOID *const left,
                                  const CW_VOID *const right);

CW_VOID *cw_data_func_u64_copy (const CW_VOID *const value);

CW_BOOL cw_data_func_u64_delete (CW_VOID *value);

CW_BOOL cw_data_func_u64_compare (const CW_VOID *const left,
                                  const CW_VOID *const right);

CW_VOID *cw_data_func_size_copy (const CW_VOID *const value);

CW_BOOL cw_data_func_size_delete (CW_VOID *value);

CW_BOOL cw_data_func_size_compare (const CW_VOID *const left,
                                   const CW_VOID *const right);

CW_VOID *cw_data_func_bool_copy (const CW_VOID *const value);

CW_BOOL cw_data_func_bool_delete (CW_VOID *value);

CW_BOOL cw_data_func_bool_compare (const CW_VOID *const left,
                                   const CW_VOID *const right);

CW_VOID *cw_data_func_string_copy (const CW_VOID *const value);

CW_BOOL cw_data_func_string_delete (CW_VOID *value);

CW_BOOL cw_data_func_string_compare (const CW_VOID *const left,
                                     const CW_VOID *const right);

typedef struct cw_data_list_t CW_DATA_LIST;

CW_DATA_LIST *cw_data_list_create (const CW_DATA_COPY copy,
                                   const CW_DATA_COMPARE compare,
                                   const CW_DATA_DELETE delete);

CW_DATA_LIST *cw_data_list_copy (const CW_DATA_LIST *const list);

CW_BOOL cw_data_list_delete (CW_DATA_LIST *list);

CW_BOOL cw_data_list_prepend (CW_DATA_LIST *const list, CW_VOID *const value);

CW_BOOL cw_data_list_append (CW_DATA_LIST *const list, CW_VOID *const value);

CW_BOOL cw_data_list_erase (CW_DATA_LIST *const list,
                            const CW_VOID *const value);

CW_BOOL cw_data_list_set (CW_DATA_LIST *const list, const CW_VOID *const match,
                          CW_VOID *const value);

CW_VOID *cw_data_list_get (CW_DATA_LIST *const list,
                           const CW_VOID *const match);

CW_BOOL cw_data_list_has (CW_DATA_LIST *const list,
                          const CW_VOID *const match);

CW_BOOL cw_data_list_count (const CW_DATA_LIST *const list, CW_SIZE *count);

CW_BOOL cw_data_list_clear (CW_DATA_LIST *const list);

CW_BOOL cw_data_list_foreach (CW_DATA_LIST *const list,
                              const CW_DATA_FOREACH foreach);

typedef struct cw_data_vector_t CW_DATA_VECTOR;

CW_DATA_VECTOR *cw_data_vector_create (const CW_DATA_COPY copy,
                                       const CW_DATA_COMPARE compare,
                                       const CW_DATA_DELETE delete,
                                       const CW_SIZE capacity);

CW_DATA_VECTOR *cw_data_vector_copy (const CW_DATA_VECTOR *const vector);

CW_BOOL cw_data_vector_delete (CW_DATA_VECTOR *vector);

CW_BOOL cw_data_vector_append (CW_DATA_VECTOR *const vector,
                               CW_VOID *const value);

CW_BOOL cw_data_vector_prepend (CW_DATA_VECTOR *const vector,
                                CW_VOID *const value);

CW_BOOL cw_data_vector_insert (CW_DATA_VECTOR *const vector,
                               const CW_SIZE index, CW_VOID *const value);

CW_VOID *cw_data_vector_pop_back (CW_DATA_VECTOR *const vector);

CW_VOID *cw_data_vector_pop_front (CW_DATA_VECTOR *const vector);

CW_BOOL cw_data_vector_remove_back (CW_DATA_VECTOR *const vector);

CW_BOOL cw_data_vector_remove_front (CW_DATA_VECTOR *const vector);

CW_VOID *cw_data_vector_pop (CW_DATA_VECTOR *const vector,
                             const CW_SIZE index);

CW_BOOL cw_data_vector_remove (CW_DATA_VECTOR *const vector,
                               const CW_SIZE index);

CW_BOOL cw_data_vector_clear (CW_DATA_VECTOR *const vector);

CW_VOID *cw_data_vector_get (CW_DATA_VECTOR *const vector,
                             const CW_SIZE index);

CW_BOOL cw_data_vector_set (CW_DATA_VECTOR *const vector, const CW_SIZE index,
                            CW_VOID *const value);

CW_BOOL cw_data_vector_size (const CW_DATA_VECTOR *const vector,
                             CW_SIZE *size);

CW_BOOL cw_data_vector_foreach (CW_DATA_VECTOR *const vector,
                                const CW_DATA_FOREACH foreach);

typedef struct cw_data_map_t CW_DATA_MAP;

CW_DATA_MAP *cw_data_map_create (const CW_DATA_COPY key_copy,
                                 const CW_DATA_COMPARE key_compare,
                                 const CW_DATA_DELETE key_delete,
                                 const CW_DATA_COPY value_copy,
                                 const CW_DATA_DELETE value_delete,
                                 const CW_DATA_HASHER hasher);

CW_DATA_MAP *cw_data_map_copy (const CW_DATA_MAP *const map);

CW_BOOL cw_data_map_delete (CW_DATA_MAP *map);

CW_BOOL cw_data_map_set (CW_DATA_MAP *const map, CW_VOID *const key,
                         CW_VOID *const value);

CW_VOID *cw_data_map_get (CW_DATA_MAP *const map, const CW_VOID *const key);

CW_BOOL cw_data_map_erase (CW_DATA_MAP *const map, const CW_VOID *const key);

CW_BOOL cw_data_map_has (CW_DATA_MAP *const map, const CW_VOID *const key);

CW_BOOL cw_data_map_size (const CW_DATA_MAP *const map, CW_SIZE *size);

CW_BOOL cw_data_map_foreach (CW_DATA_MAP *const map,
                             const CW_DATA_FOREACH_PAIR foreach);

typedef struct cw_data_set_t CW_DATA_SET;

CW_DATA_SET *cw_data_set_create (const CW_DATA_COPY copy,
                                 const CW_DATA_COMPARE compare,
                                 const CW_DATA_DELETE delete,
                                 const CW_DATA_HASHER hasher);

CW_DATA_SET *cw_data_set_copy (const CW_DATA_SET *const set);

CW_BOOL cw_data_set_delete (CW_DATA_SET *set);

#endif /* CW_DATA_H */
