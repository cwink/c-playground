#include "../inc/cw_math.h"

#include <math.h>

#define CW_MATH_EPSILON 0.0001

CW_BOOL
cw_math_compare_r32 (const CW_R32 first, const CW_R32 second)
{
  return (fabs ((double)first - (double)second) < CW_MATH_EPSILON) == 1
             ? CW_BOOL_TRUE
             : CW_BOOL_FALSE;
}

CW_BOOL
cw_math_compare_r64 (const CW_R64 first, const CW_R64 second)
{
  return (fabs (first - second) < CW_MATH_EPSILON) == 1 ? CW_BOOL_TRUE
                                                        : CW_BOOL_FALSE;
}
