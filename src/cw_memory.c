#include "../inc/cw_memory.h"
#include "../inc/cw_error.h"

#include <assert.h>
#include <string.h>

#ifndef CW_MEMORY_UNSAFE
#define CW_MEMORY_DEFAULT_MAP_SIZE 16

#define CW_MEMORY_LOAD_FACTOR 0.75

#define CW_MEMORY_LOAD_FACTOR_DIVISOR 1.5
#endif /* CW_MEMORY_UNSAFE */

#ifdef CW_MEMORY_UNSAFE
CW_BOOL cw_memory_create (CW_VOID) { return CW_BOOL_TRUE; }

CW_BOOL cw_memory_delete (CW_VOID) { return CW_BOOL_TRUE; }

CW_BOOL cw_memory_clear (CW_VOID) { return CW_BOOL_TRUE; }

void *
cw_memory_allocate (const CW_SIZE size)
{
  return malloc (size);
}

void *
cw_memory_reallocate (CW_VOID *const memory, const CW_SIZE size)
{
  return realloc (memory, size);
}

CW_BOOL
cw_memory_free (CW_VOID **const memory)
{
  CW_ERROR_CHECK (memory != NULL, CW_BOOL_FALSE,
                  "failed to free global memory map, pointer to memory "
                  "pointer argument is NULL");

  CW_ERROR_CHECK (
      *memory != NULL, CW_BOOL_FALSE,
      "failed to free global memory map, memory pointer argument is NULL");

  free (*memory);

  *memory = NULL;

  return CW_BOOL_TRUE;
}

CW_BOOL
cw_memory_is_allocated (const CW_VOID *const memory)
{
  return memory == NULL ? CW_BOOL_FALSE : CW_BOOL_TRUE;
}

CW_BOOL
cw_memory_dump (FILE *const file)
{
  CW_ERROR_CHECK (
      file != NULL, CW_BOOL_FALSE,
      "failed to dump global memory map, file argument must not be NULL");

  return CW_BOOL_TRUE;
}
#else  /* CW_MEMORY_UNSAFE */
typedef struct cw_memory_node_t
{
  CW_VOID *memory;

  CW_SIZE size;

  struct cw_memory_node_t *next;
} CW_MEMORY_NODE;

typedef struct cw_memory_t
{
  CW_MEMORY_NODE **nodes;

  CW_SIZE count;

  CW_SIZE capacity;
} CW_MEMORY;

static CW_MEMORY *cw_memory_map = NULL;

static CW_MEMORY_NODE *
cw_memory_node_create (const CW_SIZE size)
{
  CW_MEMORY_NODE *node = malloc (sizeof (CW_MEMORY_NODE));

  CW_ERROR_CHECK (node != NULL, NULL,
                  "failed to create a new memory node, new memory map node "
                  "allocation failed");

  node->memory = malloc (size);

  CW_ERROR_CHECK (node->memory != NULL, NULL,
                  "failed to create a new memory node, new memory for memory "
                  "map node failed to allocate");

  node->size = size;

  node->next = NULL;

  return node;
}

static CW_VOID
cw_memory_node_delete (CW_MEMORY_NODE *const node)
{
  assert (node != NULL);

  assert (node->memory != NULL);

  free (node->memory);

  free (node);
}

static CW_BOOL cw_memory_resize (CW_VOID)
{
  CW_SIZE index = 0;

  assert (cw_memory_map != NULL);

  assert (cw_memory_map->nodes != NULL);

  if (((CW_R64)cw_memory_map->count / (CW_R64)cw_memory_map->capacity)
      >= CW_MEMORY_LOAD_FACTOR)
    {
      CW_MEMORY_NODE **nodes
          = malloc (sizeof (CW_MEMORY_NODE *) * (cw_memory_map->capacity * 2));

      CW_ERROR_CHECK (nodes != NULL, CW_BOOL_FALSE,
                      "failed to resize global memory map, allocation of new "
                      "nodes failed");

      for (index = 0; index < cw_memory_map->capacity * 2; ++index)
        {
          nodes[index] = NULL;
        }

      for (index = 0; index < cw_memory_map->capacity; ++index)
        {
          CW_MEMORY_NODE *node = NULL;

          if (cw_memory_map->nodes[index] == NULL)
            {
              continue;
            }

          for (node = cw_memory_map->nodes[index]; node != NULL;)
            {
              CW_MEMORY_NODE *ptr = NULL;

              CW_MEMORY_NODE *next = node->next;

              CW_U64 hash
                  = (CW_SIZE)node->memory % (cw_memory_map->capacity * 2);

              if (nodes[hash] == NULL)
                {
                  nodes[hash] = node;

                  nodes[hash]->next = NULL;

                  node = next;

                  continue;
                }

              for (ptr = nodes[hash]; ptr->next != NULL; ptr = ptr->next)
                {
                }

              ptr->next = node;

              ptr->next->next = NULL;

              node = next;
            }
        }

      free (cw_memory_map->nodes);

      cw_memory_map->nodes = nodes;

      cw_memory_map->capacity *= 2;
    }

  if (((CW_R64)cw_memory_map->count / (CW_R64)cw_memory_map->capacity)
      <= (CW_MEMORY_LOAD_FACTOR / CW_MEMORY_LOAD_FACTOR_DIVISOR))
    {
      CW_SIZE new_capacity = (cw_memory_map->capacity / 2)
                             + ((cw_memory_map->capacity / 2) % 2);

      CW_MEMORY_NODE **nodes
          = malloc (sizeof (CW_MEMORY_NODE *) * new_capacity);

      CW_ERROR_CHECK (nodes != NULL, CW_BOOL_FALSE,
                      "failed to resize global memory map, allocation of new "
                      "nodes failed");

      for (index = 0; index < new_capacity; ++index)
        {
          nodes[index] = NULL;
        }

      for (index = 0; index < cw_memory_map->capacity; ++index)
        {
          CW_MEMORY_NODE *node = NULL;

          if (cw_memory_map->nodes[index] == NULL)
            {
              continue;
            }

          for (node = cw_memory_map->nodes[index]; node != NULL;)
            {
              CW_MEMORY_NODE *ptr = NULL;

              CW_MEMORY_NODE *next = node->next;

              CW_U64 hash = (CW_SIZE)node->memory % new_capacity;

              if (nodes[hash] == NULL)
                {
                  nodes[hash] = node;

                  nodes[hash]->next = NULL;

                  node = next;

                  continue;
                }

              for (ptr = nodes[hash]; ptr->next != NULL; ptr = ptr->next)
                {
                }

              ptr->next = node;

              ptr->next->next = NULL;

              node = next;
            }
        }

      free (cw_memory_map->nodes);

      cw_memory_map->nodes = nodes;

      cw_memory_map->capacity = new_capacity;
    }

  return CW_BOOL_TRUE;
}

CW_BOOL cw_memory_create (CW_VOID)
{
  CW_SIZE index = 0;

  CW_ERROR_CHECK (
      cw_memory_map == NULL, CW_BOOL_FALSE,
      "failed to create global memory map, it has already been created");

  cw_memory_map = malloc (sizeof (CW_MEMORY));

  CW_ERROR_CHECK (cw_memory_map != NULL, CW_BOOL_FALSE,
                  "failed to create global memory map, allocaton failed");

  cw_memory_map->nodes
      = malloc (sizeof (CW_MEMORY_NODE *) * CW_MEMORY_DEFAULT_MAP_SIZE);

  if (cw_memory_map->nodes == NULL)
    {
      CW_ERROR_WRITE ("failed to create global memory map, allocation of "
                      "memory nodes failed");

      free (cw_memory_map);

      cw_memory_map = NULL;

      return CW_BOOL_FALSE;
    }

  for (index = 0; index < CW_MEMORY_DEFAULT_MAP_SIZE; ++index)
    {
      cw_memory_map->nodes[index] = NULL;
    }

  cw_memory_map->count = 0;

  cw_memory_map->capacity = CW_MEMORY_DEFAULT_MAP_SIZE;

  return CW_BOOL_TRUE;
}

CW_BOOL cw_memory_delete (CW_VOID)
{
  CW_SIZE index = 0;

  CW_ERROR_CHECK (
      cw_memory_map != NULL, CW_BOOL_FALSE,
      "failed to delete global memory map, it has not been created yet");

  for (index = 0; index < cw_memory_map->capacity; ++index)
    {
      CW_MEMORY_NODE *node = NULL;

      for (node = cw_memory_map->nodes[index]; node != NULL;)
        {
          CW_MEMORY_NODE *next = node->next;

          cw_memory_node_delete (node);

          node = next;
        }
    }

  free (cw_memory_map->nodes);

  free (cw_memory_map);

  cw_memory_map = NULL;

  return CW_BOOL_TRUE;
}

CW_BOOL cw_memory_clear (CW_VOID)
{
  CW_ERROR_CHECK (cw_memory_delete () != CW_BOOL_FALSE, CW_BOOL_FALSE,
                  "failed to clear global memory map, deletion failed");

  CW_ERROR_CHECK (cw_memory_create () != CW_BOOL_FALSE, CW_BOOL_FALSE,
                  "failed to clear global memory map, creation failed");

  return CW_BOOL_TRUE;
}

CW_VOID *
cw_memory_allocate (const CW_SIZE size)
{
  CW_MEMORY_NODE *node = NULL;

  CW_MEMORY_NODE *ptr = NULL;

  CW_U64 hash = 0UL;

  CW_ERROR_CHECK (cw_memory_map != NULL, NULL,
                  "failed to allocate new memory, global memory map host not "
                  "been created yet");

  node = cw_memory_node_create (size);

  CW_ERROR_CHECK (
      node != NULL, NULL,
      "failed to allocate new memory, creation of new node failed");

  hash = (CW_SIZE)node->memory % cw_memory_map->capacity;

  if (cw_memory_map->nodes[hash] == NULL)
    {
      cw_memory_map->nodes[hash] = node;

      ++cw_memory_map->count;

      CW_ERROR_CHECK (cw_memory_resize () != CW_BOOL_FALSE, NULL,
                      "failed to allocate new memory, resizing of global "
                      "memory map failed");

      return node->memory;
    }

  ptr = cw_memory_map->nodes[hash];

  if (ptr->memory == node->memory)
    {
      CW_MEMORY_NODE *next = ptr->next;

      cw_memory_node_delete (ptr);

      node->next = next;

      cw_memory_map->nodes[hash] = node;

      ++cw_memory_map->count;

      CW_ERROR_CHECK (cw_memory_resize () != CW_BOOL_FALSE, NULL,
                      "failed to allocate new memory, resizing of global "
                      "memory map failed");

      return node->memory;
    }

  for (; ptr->next != NULL; ptr = ptr->next)
    {
      if (ptr->next->memory == node->memory)
        {
          CW_MEMORY_NODE *next = ptr->next->next;

          cw_memory_node_delete (ptr->next);

          node->next = next;

          ptr->next = node;

          ++cw_memory_map->count;

          CW_ERROR_CHECK (cw_memory_resize () != CW_BOOL_FALSE, NULL,
                          "failed to allocate new memory, resizing of global "
                          "memory map failed");

          return node->memory;
        }
    }

  ptr->next = node;

  ++cw_memory_map->count;

  CW_ERROR_CHECK (
      cw_memory_resize () != CW_BOOL_FALSE, NULL,
      "failed to allocate new memory, resizing of global memory map failed");

  return node->memory;
}

CW_VOID *
cw_memory_reallocate (CW_VOID *const memory, const CW_SIZE size)
{
  CW_MEMORY_NODE *node = NULL;

  CW_U64 hash = 0UL;

  CW_ERROR_CHECK (
      memory != NULL, NULL,
      "failed to reallocate memory, memory argument must not be NULL");

  CW_ERROR_CHECK (cw_memory_map != NULL, NULL,
                  "failed to reallocate memory, global memory map host not "
                  "been created yet");

  hash = (CW_SIZE)memory % cw_memory_map->capacity;

  for (node = cw_memory_map->nodes[hash]; node != NULL; node = node->next)
    {
      if (node->memory == memory)
        {
          CW_VOID *new_memory = realloc (node->memory, size);

          CW_ERROR_CHECK (new_memory != NULL, NULL,
                          "failed to reallocate memory, memory reallocation "
                          "in node failed");

          node->memory = new_memory;

          return new_memory;
        }
    }

  CW_ERROR_WRITE ("failed to reallocate memory, unable to find memory in "
                  "global memory map");

  return NULL;
}

CW_BOOL
cw_memory_free (CW_VOID **const memory)
{
  CW_MEMORY_NODE *ptr = NULL;

  CW_U64 hash = 0UL;

  CW_ERROR_CHECK (
      memory != NULL, CW_BOOL_FALSE,
      "failed to free memory, pointer to memory pointer argument is NULL");

  CW_ERROR_CHECK (*memory != NULL, CW_BOOL_FALSE,
                  "failed to free memory, memory pointer argument is NULL");

  CW_ERROR_CHECK (
      cw_memory_map != NULL, CW_BOOL_FALSE,
      "failed to free memory, global memory map has not been created yet");

  hash = (CW_SIZE) (*memory) % cw_memory_map->capacity;

  CW_ERROR_CHECK_1 (
      cw_memory_map->nodes[hash] != NULL, CW_BOOL_FALSE,
      "failed to free memory, unable to find memory with address '%p'",
      (*memory));

  ptr = cw_memory_map->nodes[hash];

  if (ptr->memory == (*memory))
    {
      CW_MEMORY_NODE *next = ptr->next;

      cw_memory_node_delete (ptr);

      cw_memory_map->nodes[hash] = next;

      *memory = NULL;

      --cw_memory_map->count;

      CW_ERROR_CHECK (
          cw_memory_resize () != CW_BOOL_FALSE, CW_BOOL_FALSE,
          "failed to free memory, resizing of global memory map failed");

      return CW_BOOL_TRUE;
    }

  for (; ptr->next != NULL; ptr = ptr->next)
    {
      if (ptr->next->memory == (*memory))
        {
          CW_MEMORY_NODE *next = ptr->next->next;

          cw_memory_node_delete (ptr->next);

          ptr->next = next;

          *memory = NULL;

          --cw_memory_map->count;

          CW_ERROR_CHECK (
              cw_memory_resize () != CW_BOOL_FALSE, CW_BOOL_FALSE,
              "failed to free memory, resizing of global memory map failed");

          return CW_BOOL_TRUE;
        }
    }

  CW_ERROR_WRITE_1 (
      "failed to free memory, unable to find memory with address '%p'",
      (*memory));

  return CW_BOOL_FALSE;
}

CW_BOOL
cw_memory_is_allocated (const CW_VOID *const memory)
{
  CW_MEMORY_NODE *ptr = NULL;

  CW_U64 hash = 0UL;

  CW_ERROR_CHECK (memory != NULL, CW_BOOL_FALSE,
                  "failed to check if memory is allocated, memory pointer "
                  "argument is NULL");

  CW_ERROR_CHECK (cw_memory_map != NULL, CW_BOOL_FALSE,
                  "failed to check if memory is allocated, global memory map "
                  "has not been created yet");

  hash = (CW_SIZE)memory % cw_memory_map->capacity;

  for (ptr = cw_memory_map->nodes[hash]; ptr != NULL; ptr = ptr->next)
    {
      if (ptr->memory == memory)
        {
          return CW_BOOL_TRUE;
        }
    }

  return CW_BOOL_FALSE;
}

CW_BOOL
cw_memory_dump (FILE *const file)
{
  CW_SIZE index = 0;

  CW_MEMORY_NODE *node = NULL;

  CW_ERROR_CHECK (
      file != NULL, CW_BOOL_FALSE,
      "failed to dump global memory map, file argument must not be NULL");

  CW_ERROR_CHECK (cw_memory_map != NULL, CW_BOOL_FALSE,
                  "failed to dump global memory map, global memory map has "
                  "not been created yet");

  fprintf (file, "[MEMORY DUMP]\n\n");

  fprintf (file, "  Capacity: %lu\n", cw_memory_map->capacity);

  fprintf (file, "  Count: %lu\n\n", cw_memory_map->count);

  for (index = 0; index < cw_memory_map->capacity; ++index)
    {
      if (cw_memory_map->nodes[index] == NULL)
        {
          continue;
        }

      for (node = cw_memory_map->nodes[index]; node != NULL; node = node->next)
        {
          CW_SIZE i = 0;

          fprintf (file, "    %p :", node->memory);

          for (i = 0; i < node->size; ++i)
            {
              fprintf (file, " 0x%2.2x",
                       ((const CW_U8 *const)node->memory)[i]);
            }

          fprintf (file, " \"");

          for (i = 0; i < node->size; ++i)
            {
              CW_CHAR character = ((const CW_CHAR *const)node->memory)[i];

              if (character == '\n' || character == '\r' || character == '\t'
                  || character == '\v' || character == '\b')
                {
                  fprintf (file, "...");

                  continue;
                }

              fprintf (file, "%c", character);
            }

          fprintf (file, "\"\n");
        }
    }

  return CW_BOOL_TRUE;
}
#endif /* CW_MEMORY_UNSAFE */
