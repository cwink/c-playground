#include "../inc/cw.h"

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int
main (void)
{
  CW_DATA_SET *set = NULL;

  cw_memory_create ();

  set = cw_data_set_create (cw_data_func_s32_copy, cw_data_func_s32_compare,
                            cw_data_func_s32_delete, cw_hash_integer);

  cw_data_set_delete (set);

  cw_memory_dump (stdout);

  cw_memory_delete ();

  return EXIT_SUCCESS;
}
