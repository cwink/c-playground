#include "../inc/cw_data.h"
#include "../inc/cw_error.h"
#include "../inc/cw_memory.h"

#include <assert.h>

typedef struct cw_data_list_node_t
{
  CW_VOID *value;

  struct cw_data_list_node_t *next;
} CW_DATA_LIST_NODE;

struct cw_data_list_t
{
  CW_DATA_COPY copy;

  CW_DATA_DELETE delete;

  CW_DATA_COMPARE compare;

  CW_DATA_LIST_NODE *root;
};

static CW_DATA_LIST_NODE *
cw_data_list_node_create (CW_VOID *const value, CW_DATA_LIST_NODE *const next)
{
  CW_DATA_LIST_NODE *node = NULL;

  assert (value != NULL);

  node = cw_memory_allocate (sizeof (CW_DATA_LIST_NODE));

  CW_ERROR_CHECK (node != NULL, NULL,
                  "failed to create a new list node, allocation of a new list "
                  "node failed");

  node->value = value;

  node->next = next;

  return node;
}

static CW_DATA_LIST_NODE *
cw_data_list_node_copy (const CW_DATA_COPY copy,
                        const CW_DATA_LIST_NODE *const node)
{
  CW_VOID *value = NULL;

  assert (copy != NULL);

  assert (node != NULL);

  value = copy (node->value);

  CW_ERROR_CHECK (value != NULL, NULL,
                  "failed to copy a list node, copy of value for node failed");

  return cw_data_list_node_create (value, NULL);
}

static CW_BOOL
cw_data_list_node_delete (const CW_DATA_DELETE delete, CW_DATA_LIST_NODE *node)
{
  assert (delete != NULL);

  assert (node != NULL);

  CW_ERROR_CHECK ((delete (node->value)) != CW_BOOL_FALSE, CW_BOOL_FALSE,
                  "failed to delete list node, deletion a value failed");

  CW_ERROR_CHECK (cw_memory_free ((CW_VOID * *const) & node) != CW_BOOL_FALSE,
                  CW_BOOL_FALSE,
                  "failed to delete list node, failed to free node memory");

  return CW_BOOL_TRUE;
}

CW_DATA_LIST *
cw_data_list_create (const CW_DATA_COPY copy, const CW_DATA_COMPARE compare,
                     const CW_DATA_DELETE delete)
{
  CW_DATA_LIST *list = NULL;

  CW_ERROR_CHECK (
      copy != NULL, NULL,
      "failed to create new list, copy function argument must not be NULL");

  CW_ERROR_CHECK (
      compare != NULL, NULL,
      "failed to create new list, compare function argument must not be NULL");

  CW_ERROR_CHECK (
      delete != NULL, NULL,
      "failed to create new list, delete function argument must not be NULL");

  list = cw_memory_allocate (sizeof (CW_DATA_LIST));

  CW_ERROR_CHECK (list != NULL, NULL,
                  "failed to create new list, allocating a new list failed");

  list->copy = copy;

  list->compare = compare;

  list->delete = delete;

  list->root = NULL;

  return list;
}

CW_DATA_LIST *
cw_data_list_copy (const CW_DATA_LIST *const list)
{
  CW_DATA_LIST *new_list = NULL;

  CW_DATA_LIST_NODE *list_next = NULL;

  CW_DATA_LIST_NODE *next = NULL;

  CW_ERROR_CHECK (list != NULL, NULL,
                  "failed to copy list, list argument must not be NULL");

  new_list = cw_data_list_create (list->copy, list->compare, list->delete);

  CW_ERROR_CHECK (new_list != NULL, NULL,
                  "failed to copy list, creating a new list failed");

  if (list->root == NULL)
    {
      return new_list;
    }

  new_list->root = cw_data_list_node_copy (list->copy, list->root);

  CW_ERROR_CHECK (new_list->root != NULL, NULL,
                  "failed to copy list, copy of the root list node failed");

  next = new_list->root;

  for (list_next = list->root->next; list_next != NULL;
       list_next = list_next->next)
    {
      next->next = cw_data_list_node_copy (list->copy, list_next);

      CW_ERROR_CHECK (next->next != NULL, NULL,
                      "failed to copy list, copy of list node failed");

      next = next->next;
    }

  return new_list;
}

CW_BOOL
cw_data_list_delete (CW_DATA_LIST *list)
{
  CW_DATA_LIST_NODE *next = NULL;

  CW_ERROR_CHECK (list != NULL, CW_BOOL_FALSE,
                  "failed to delete list, list argument must not be NULL");

  for (next = list->root; next != NULL;)
    {
      CW_DATA_LIST_NODE *tmp = next->next;

      CW_ERROR_CHECK (
          cw_data_list_node_delete (list->delete, next) != CW_BOOL_FALSE,
          CW_BOOL_FALSE,
          "failed to delete list, deletion of node in list failed");

      next = tmp;
    }

  CW_ERROR_CHECK (cw_memory_free ((CW_VOID * *const) & list) != CW_BOOL_FALSE,
                  CW_BOOL_FALSE,
                  "failed to delete list, failed to free list memory");

  return CW_BOOL_TRUE;
}

CW_BOOL
cw_data_list_prepend (CW_DATA_LIST *const list, CW_VOID *const value)
{
  CW_DATA_LIST_NODE *node = NULL;

  CW_DATA_LIST_NODE *next = NULL;

  CW_ERROR_CHECK (list != NULL, CW_BOOL_FALSE,
                  "failed to prepend to list, list argument must not be NULL");

  CW_ERROR_CHECK (
      value != NULL, CW_BOOL_FALSE,
      "failed to prepend to list, value argument must not be NULL");

  node = cw_data_list_node_create (value, NULL);

  CW_ERROR_CHECK (
      node != NULL, CW_BOOL_FALSE,
      "failed to prepend to list, creation of new node in list failed");

  next = list->root;

  list->root = node;

  list->root->next = next;

  return CW_BOOL_TRUE;
}

CW_BOOL
cw_data_list_append (CW_DATA_LIST *const list, CW_VOID *const value)
{
  CW_DATA_LIST_NODE *node = NULL;

  CW_DATA_LIST_NODE *next = NULL;

  CW_ERROR_CHECK (list != NULL, CW_BOOL_FALSE,
                  "failed to append to list, list must not be NULL");

  CW_ERROR_CHECK (value != NULL, CW_BOOL_FALSE,
                  "failed to append to list, value must not be NULL");

  node = cw_data_list_node_create (value, NULL);

  CW_ERROR_CHECK (
      node != NULL, CW_BOOL_FALSE,
      "failed to append to list, creation of a new node in list failed");

  if (list->root == NULL)
    {
      list->root = node;

      return CW_BOOL_TRUE;
    }

  for (next = list->root;; next = next->next)
    {
      if (next->next == NULL)
        {
          break;
        }
    }

  next->next = node;

  return CW_BOOL_TRUE;
}

CW_BOOL
cw_data_list_erase (CW_DATA_LIST *const list, const CW_VOID *const value)
{
  CW_DATA_LIST_NODE *next = NULL;

  CW_ERROR_CHECK (list != NULL, CW_BOOL_FALSE,
                  "failed to erase from list, list argument must not be NULL");

  CW_ERROR_CHECK (
      value != NULL, CW_BOOL_FALSE,
      "failed to erase from list, value argument must not be NULL");

  if (list->root != NULL)
    {
      if (list->compare (list->root->value, value) == CW_BOOL_TRUE)
        {
          next = list->root->next;

          CW_ERROR_CHECK (cw_data_list_node_delete (list->delete, list->root)
                              != CW_BOOL_FALSE,
                          CW_BOOL_FALSE,
                          "failed to erase from list, deletion of root node "
                          "in list failed");

          list->root = next;

          return CW_BOOL_TRUE;
        }

      for (next = list->root; next->next != NULL; next = next->next)
        {
          if (list->compare (next->next->value, value) == CW_BOOL_TRUE)
            {
              CW_DATA_LIST_NODE *tmp = next->next->next;

              CW_ERROR_CHECK (
                  cw_data_list_node_delete (list->delete, next->next)
                      != CW_BOOL_FALSE,
                  CW_BOOL_FALSE,
                  "failed to erase from list, deletion of a node in the list "
                  "failed");

              next->next = tmp;

              return CW_BOOL_TRUE;
            }
        }
    }

  CW_ERROR_WRITE ("failed to erase from list, value not found in list");

  return CW_BOOL_FALSE;
}

CW_BOOL
cw_data_list_set (CW_DATA_LIST *const list, const CW_VOID *const match,
                  CW_VOID *const value)
{
  CW_DATA_LIST_NODE *next = NULL;

  CW_ERROR_CHECK (
      list != NULL, CW_BOOL_FALSE,
      "failed to set value in list, list argument must not be NULL");

  CW_ERROR_CHECK (
      match != NULL, CW_BOOL_FALSE,
      "failed to set value in list, match argument must not be NULL");

  CW_ERROR_CHECK (
      value != NULL, CW_BOOL_FALSE,
      "failed to set value in list, value argument must not be NULL");

  if (list->root == NULL)
    {
      if (list->compare (list->root->value, match) == CW_BOOL_TRUE)
        {
          next = list->root->next;

          CW_ERROR_CHECK (cw_data_list_node_delete (list->delete, list->root)
                              != CW_BOOL_FALSE,
                          CW_BOOL_FALSE,
                          "failed to set value in list, deletion of root node "
                          "in list failed");

          list->root = cw_data_list_node_create (value, next);

          CW_ERROR_CHECK (
              list->root != NULL, CW_BOOL_FALSE,
              "failed to set value in list, creation of new list node failed");

          return CW_BOOL_TRUE;
        }

      for (next = list->root; next->next != NULL; next = next->next)
        {
          if (list->compare (next->next->value, match) == CW_BOOL_TRUE)
            {
              CW_DATA_LIST_NODE *tmp = next->next->next;

              CW_ERROR_CHECK (
                  cw_data_list_node_delete (list->delete, next->next)
                      != CW_BOOL_FALSE,
                  CW_BOOL_FALSE,
                  "failed to set value in list, deletion of a node in list "
                  "failed");

              next->next = cw_data_list_node_create (value, tmp);

              CW_ERROR_CHECK (next->next != NULL, CW_BOOL_FALSE,
                              "failed to set value in list, creation of a new "
                              "list node failed");

              return CW_BOOL_TRUE;
            }
        }
    }

  CW_ERROR_WRITE ("failed to set value in list, value not found in list");

  return CW_BOOL_FALSE;
}

CW_VOID *
cw_data_list_get (CW_DATA_LIST *const list, const CW_VOID *const match)
{
  CW_DATA_LIST_NODE *next = NULL;

  CW_ERROR_CHECK (
      list != NULL, NULL,
      "failed to get value from list, list argument must not be NULL");

  CW_ERROR_CHECK (
      match != NULL, NULL,
      "failed to get value from list, match argument must not be NULL");

  for (next = list->root; next != NULL; next = next->next)
    {
      if (list->compare (next->value, match) == CW_BOOL_TRUE)
        {
          return next->value;
        }
    }

  return NULL;
}

CW_BOOL
cw_data_list_has (CW_DATA_LIST *const list, const CW_VOID *const match)
{
  CW_DATA_LIST_NODE *next = NULL;

  CW_ERROR_CHECK (
      list != NULL, CW_BOOL_FALSE,
      "failed to check if value is in list, list argument must not be NULL");

  CW_ERROR_CHECK (
      match != NULL, CW_BOOL_FALSE,
      "failed to check if value is in list, match argument must not be NULL");

  for (next = list->root; next != NULL; next = next->next)
    {
      if (list->compare (next->value, match) == CW_BOOL_TRUE)
        {
          return CW_BOOL_TRUE;
        }
    }

  return CW_BOOL_FALSE;
}

CW_BOOL
cw_data_list_count (const CW_DATA_LIST *const list, CW_SIZE *count)
{
  CW_DATA_LIST_NODE *next = NULL;

  CW_ERROR_CHECK (
      list != NULL, CW_BOOL_FALSE,
      "failed to count values in list, list argument must not be NULL");

  CW_ERROR_CHECK (
      count != NULL, CW_BOOL_FALSE,
      "failed to count values in list, match argument must not be NULL");

  *count = 0;

  for (next = list->root; next != NULL; next = next->next)
    {
      ++(*count);
    }

  return CW_BOOL_TRUE;
}

CW_BOOL
cw_data_list_clear (CW_DATA_LIST *const list)
{
  CW_DATA_LIST_NODE *ptr = NULL;

  CW_ERROR_CHECK (list != NULL, CW_BOOL_FALSE,
                  "failed to clear list, list argument must not be NULL");

  for (ptr = list->root; ptr != NULL;)
    {
      CW_DATA_LIST_NODE *next = ptr->next;

      CW_ERROR_CHECK (
          cw_data_list_node_delete (list->delete, ptr) != CW_BOOL_FALSE,
          CW_BOOL_FALSE, "failed to clear list, deletion of list node failed");

      ptr = next;
    }

  list->root = NULL;

  return CW_BOOL_TRUE;
}

CW_BOOL
cw_data_list_foreach (CW_DATA_LIST *const list, const CW_DATA_FOREACH foreach)
{
  CW_DATA_LIST_NODE *next = NULL;

  CW_ERROR_CHECK (
      list != NULL, CW_BOOL_FALSE,
      "failed to loop through list, list argument must not be NULL");

  CW_ERROR_CHECK (foreach != NULL, CW_BOOL_FALSE,
                  "failed to loop through list, foreach function argument "
                  "must not be NULL");

  for (next = list->root; next != NULL; next = next->next)
    {
      CW_BOOL result = foreach (next->value);

      if (result != CW_BOOL_TRUE)
        {
          return CW_BOOL_FALSE;
        }
    }

  return CW_BOOL_TRUE;
}
