#include "../inc/cw_error.h"

#include <stdio.h>
#include <time.h>

#define CW_ERROR_TIME_LENGTH 32

static CW_VOID
cw_error_callback_default (const CW_CHAR *const filename, const CW_SIZE line,
                           const CW_CHAR *const message)
{
  char buffer[CW_ERROR_TIME_LENGTH];

  time_t current_time = time (NULL);

  strftime (buffer, CW_ERROR_TIME_LENGTH, "%Y-%m-%d %H:%M:%S",
            localtime (&current_time));

  fprintf (stderr, "[%s][%s:%lu] %s\n", buffer, filename, line, message);
}

static CW_ERROR_CALLBACK cw_error_callback = cw_error_callback_default;

CW_VOID
cw_error_callback_set (const CW_ERROR_CALLBACK callback)
{
  if (callback == NULL)
    {
      return;
    }

  cw_error_callback = callback;
}

CW_VOID
cw_error_write (const CW_CHAR *const filename, const CW_SIZE line,
                const CW_CHAR *const format, ...)
{
  va_list arguments;

  char buffer[CW_ERROR_MAX_MESSAGE_LENGTH];

  va_start (arguments, format);

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wformat-nonliteral"
  vsprintf (buffer, format, arguments);
#pragma GCC diagnostic pop

  va_end (arguments);

  cw_error_callback (filename, line, buffer);
}
