#include "../inc/cw_data.h"
#include "../inc/cw_error.h"
#include "../inc/cw_memory.h"

#include <string.h>

CW_VOID *
cw_data_func_char_copy (const CW_VOID *const value)
{
  CW_CHAR *new_value = NULL;

  CW_ERROR_CHECK (value != NULL, NULL,
                  "failed to copy CW_CHAR, value argument must not be NULL");

  new_value = cw_memory_allocate (sizeof (CW_CHAR));

  CW_ERROR_CHECK (new_value != NULL, NULL,
                  "failed to copy CW_CHAR, memory allocation failed");

  *new_value = *((const CW_CHAR *const)value);

  return new_value;
}

CW_BOOL
cw_data_func_char_delete (CW_VOID *value)
{
  CW_ERROR_CHECK (value != NULL, CW_BOOL_FALSE,
                  "failed to delete CW_CHAR, value argument must not be NULL");

  CW_ERROR_CHECK (cw_memory_free (&value) != CW_BOOL_FALSE, CW_BOOL_FALSE,
                  "failed to delete CW_CHAR, freeing of memory failed");

  return CW_BOOL_TRUE;
}

CW_BOOL
cw_data_func_char_compare (const CW_VOID *const left,
                           const CW_VOID *const right)
{
  CW_ERROR_CHECK (left != NULL, CW_BOOL_FALSE,
                  "failed to compare CW_CHAR, left argument must not be NULL");

  CW_ERROR_CHECK (
      right != NULL, CW_BOOL_FALSE,
      "failed to compare CW_CHAR, right argument must not be NULL");

  if (*((const CW_CHAR *const)left) == *((const CW_CHAR *const)right))
    {
      return CW_BOOL_TRUE;
    }

  return CW_BOOL_FALSE;
}

CW_VOID *
cw_data_func_s8_copy (const CW_VOID *const value)
{
  CW_S8 *new_value = NULL;

  CW_ERROR_CHECK (value != NULL, NULL,
                  "failed to copy CW_S8, value argument must not be NULL");

  new_value = cw_memory_allocate (sizeof (CW_S8));

  CW_ERROR_CHECK (new_value != NULL, NULL,
                  "failed to copy CW_S8, memory allocation failed");

  *new_value = *((const CW_S8 *const)value);

  return new_value;
}

CW_BOOL
cw_data_func_s8_delete (CW_VOID *value)
{
  CW_ERROR_CHECK (value != NULL, CW_BOOL_FALSE,
                  "failed to delete CW_S8, value argument must not be NULL");

  CW_ERROR_CHECK (cw_memory_free (&value) != CW_BOOL_FALSE, CW_BOOL_FALSE,
                  "failed to delete CW_S8, freeing of memory failed");

  return CW_BOOL_TRUE;
}

CW_BOOL
cw_data_func_s8_compare (const CW_VOID *const left, const CW_VOID *const right)
{
  CW_ERROR_CHECK (left != NULL, CW_BOOL_FALSE,
                  "failed to compare CW_S8, left argument must not be NULL");

  CW_ERROR_CHECK (right != NULL, CW_BOOL_FALSE,
                  "failed to compare CW_S8, right argument must not be NULL");

  if (*((const CW_S8 *const)left) == *((const CW_S8 *const)right))
    {
      return CW_BOOL_TRUE;
    }

  return CW_BOOL_FALSE;
}

CW_VOID *
cw_data_func_u8_copy (const CW_VOID *const value)
{
  CW_U8 *new_value = NULL;

  CW_ERROR_CHECK (value != NULL, NULL,
                  "failed to copy CW_U8, value argument must not be NULL");

  new_value = cw_memory_allocate (sizeof (CW_U8));

  CW_ERROR_CHECK (new_value != NULL, NULL,
                  "failed to copy CW_U8, memory allocation failed");

  *new_value = *((const CW_U8 *const)value);

  return new_value;
}

CW_BOOL
cw_data_func_u8_delete (CW_VOID *value)
{
  CW_ERROR_CHECK (value != NULL, CW_BOOL_FALSE,
                  "failed to delete CW_U8, value argument must not be NULL");

  CW_ERROR_CHECK (cw_memory_free (&value) != CW_BOOL_FALSE, CW_BOOL_FALSE,
                  "failed to delete CW_U8, freeing of memory failed");

  return CW_BOOL_TRUE;
}

CW_BOOL
cw_data_func_u8_compare (const CW_VOID *const left, const CW_VOID *const right)
{
  CW_ERROR_CHECK (left != NULL, CW_BOOL_FALSE,
                  "failed to compare CW_U8, left argument must not be NULL");

  CW_ERROR_CHECK (right != NULL, CW_BOOL_FALSE,
                  "failed to compare CW_U8, right argument must not be NULL");

  if (*((const CW_U8 *const)left) == *((const CW_U8 *const)right))
    {
      return CW_BOOL_TRUE;
    }

  return CW_BOOL_FALSE;
}

CW_VOID *
cw_data_func_s16_copy (const CW_VOID *const value)
{
  CW_S16 *new_value = NULL;

  CW_ERROR_CHECK (value != NULL, NULL,
                  "failed to copy CW_S16, value argument must not be NULL");

  new_value = cw_memory_allocate (sizeof (CW_S16));

  CW_ERROR_CHECK (new_value != NULL, NULL,
                  "failed to copy CW_S16, memory allocation failed");

  *new_value = *((const CW_S16 *const)value);

  return new_value;
}

CW_BOOL
cw_data_func_s16_delete (CW_VOID *value)
{
  CW_ERROR_CHECK (value != NULL, CW_BOOL_FALSE,
                  "failed to delete CW_S16, value argument must not be NULL");

  CW_ERROR_CHECK (cw_memory_free (&value) != CW_BOOL_FALSE, CW_BOOL_FALSE,
                  "failed to delete CW_S16, freeing of memory failed");

  return CW_BOOL_TRUE;
}

CW_BOOL
cw_data_func_s16_compare (const CW_VOID *const left,
                          const CW_VOID *const right)
{
  CW_ERROR_CHECK (left != NULL, CW_BOOL_FALSE,
                  "failed to compare CW_S16, left argument must not be NULL");

  CW_ERROR_CHECK (right != NULL, CW_BOOL_FALSE,
                  "failed to compare CW_S16, right argument must not be NULL");

  if (*((const CW_S16 *const)left) == *((const CW_S16 *const)right))
    {
      return CW_BOOL_TRUE;
    }

  return CW_BOOL_FALSE;
}

CW_VOID *
cw_data_func_u16_copy (const CW_VOID *const value)
{
  CW_U16 *new_value = NULL;

  CW_ERROR_CHECK (value != NULL, NULL,
                  "failed to copy CW_U16, value argument must not be NULL");

  new_value = cw_memory_allocate (sizeof (CW_U16));

  CW_ERROR_CHECK (new_value != NULL, NULL,
                  "failed to copy CW_U16, memory allocation failed");

  *new_value = *((const CW_U16 *const)value);

  return new_value;
}

CW_BOOL
cw_data_func_u16_delete (CW_VOID *value)
{
  CW_ERROR_CHECK (value != NULL, CW_BOOL_FALSE,
                  "failed to delete CW_U16, value argument must not be NULL");

  CW_ERROR_CHECK (cw_memory_free (&value) != CW_BOOL_FALSE, CW_BOOL_FALSE,
                  "failed to delete CW_U16, freeing of memory failed");

  return CW_BOOL_TRUE;
}

CW_BOOL
cw_data_func_u16_compare (const CW_VOID *const left,
                          const CW_VOID *const right)
{
  CW_ERROR_CHECK (left != NULL, CW_BOOL_FALSE,
                  "failed to compare CW_U16, left argument must not be NULL");

  CW_ERROR_CHECK (right != NULL, CW_BOOL_FALSE,
                  "failed to compare CW_U16, right argument must not be NULL");

  if (*((const CW_U16 *const)left) == *((const CW_U16 *const)right))
    {
      return CW_BOOL_TRUE;
    }

  return CW_BOOL_FALSE;
}

CW_VOID *
cw_data_func_s32_copy (const CW_VOID *const value)
{
  CW_S32 *new_value = NULL;

  CW_ERROR_CHECK (value != NULL, NULL,
                  "failed to copy CW_S32, value argument must not be NULL");

  new_value = cw_memory_allocate (sizeof (CW_S32));

  CW_ERROR_CHECK (new_value != NULL, NULL,
                  "failed to copy CW_S32, memory allocation failed");

  *new_value = *((const CW_S32 *const)value);

  return new_value;
}

CW_BOOL
cw_data_func_s32_delete (CW_VOID *value)
{
  CW_ERROR_CHECK (value != NULL, CW_BOOL_FALSE,
                  "failed to delete CW_S32, value argument must not be NULL");

  CW_ERROR_CHECK (cw_memory_free (&value) != CW_BOOL_FALSE, CW_BOOL_FALSE,
                  "failed to delete CW_S32, freeing of memory failed");

  return CW_BOOL_TRUE;
}

CW_BOOL
cw_data_func_s32_compare (const CW_VOID *const left,
                          const CW_VOID *const right)
{
  CW_ERROR_CHECK (left != NULL, CW_BOOL_FALSE,
                  "failed to compare CW_S32, left argument must not be NULL");

  CW_ERROR_CHECK (right != NULL, CW_BOOL_FALSE,
                  "failed to compare CW_S32, right argument must not be NULL");

  if (*((const CW_S32 *const)left) == *((const CW_S32 *const)right))
    {
      return CW_BOOL_TRUE;
    }

  return CW_BOOL_FALSE;
}

CW_VOID *
cw_data_func_u32_copy (const CW_VOID *const value)
{
  CW_U32 *new_value = NULL;

  CW_ERROR_CHECK (value != NULL, NULL,
                  "failed to copy CW_U32, value argument must not be NULL");

  new_value = cw_memory_allocate (sizeof (CW_U32));

  CW_ERROR_CHECK (new_value != NULL, NULL,
                  "failed to copy CW_U32, memory allocation failed");

  *new_value = *((const CW_U32 *const)value);

  return new_value;
}

CW_BOOL
cw_data_func_u32_delete (CW_VOID *value)
{
  CW_ERROR_CHECK (value != NULL, CW_BOOL_FALSE,
                  "failed to delete CW_U32, value argument must not be NULL");

  CW_ERROR_CHECK (cw_memory_free (&value) != CW_BOOL_FALSE, CW_BOOL_FALSE,
                  "failed to delete CW_U32, freeing of memory failed");

  return CW_BOOL_TRUE;
}

CW_BOOL
cw_data_func_u32_compare (const CW_VOID *const left,
                          const CW_VOID *const right)
{
  CW_ERROR_CHECK (left != NULL, CW_BOOL_FALSE,
                  "failed to compare CW_U32, left argument must not be NULL");

  CW_ERROR_CHECK (right != NULL, CW_BOOL_FALSE,
                  "failed to compare CW_U32, right argument must not be NULL");

  if (*((const CW_U32 *const)left) == *((const CW_U32 *const)right))
    {
      return CW_BOOL_TRUE;
    }

  return CW_BOOL_FALSE;
}

CW_VOID *
cw_data_func_s64_copy (const CW_VOID *const value)
{
  CW_S64 *new_value = NULL;

  CW_ERROR_CHECK (value != NULL, NULL,
                  "failed to copy CW_S64, value argument must not be NULL");

  new_value = cw_memory_allocate (sizeof (CW_S64));

  CW_ERROR_CHECK (new_value != NULL, NULL,
                  "failed to copy CW_S64, memory allocation failed");

  *new_value = *((const CW_S64 *const)value);

  return new_value;
}

CW_BOOL
cw_data_func_s64_delete (CW_VOID *value)
{
  CW_ERROR_CHECK (value != NULL, CW_BOOL_FALSE,
                  "failed to delete CW_S64, value argument must not be NULL");

  CW_ERROR_CHECK (cw_memory_free (&value) != CW_BOOL_FALSE, CW_BOOL_FALSE,
                  "failed to delete CW_S64, freeing of memory failed");

  return CW_BOOL_TRUE;
}

CW_BOOL
cw_data_func_s64_compare (const CW_VOID *const left,
                          const CW_VOID *const right)
{
  CW_ERROR_CHECK (left != NULL, CW_BOOL_FALSE,
                  "failed to compare CW_S64, left argument must not be NULL");

  CW_ERROR_CHECK (right != NULL, CW_BOOL_FALSE,
                  "failed to compare CW_S64, right argument must not be NULL");

  if (*((const CW_S64 *const)left) == *((const CW_S64 *const)right))
    {
      return CW_BOOL_TRUE;
    }

  return CW_BOOL_FALSE;
}

CW_VOID *
cw_data_func_u64_copy (const CW_VOID *const value)
{
  CW_U64 *new_value = NULL;

  CW_ERROR_CHECK (value != NULL, NULL,
                  "failed to copy CW_U64, value argument must not be NULL");

  new_value = cw_memory_allocate (sizeof (CW_U64));

  CW_ERROR_CHECK (new_value != NULL, NULL,
                  "failed to copy CW_U64, memory allocation failed");

  *new_value = *((const CW_U64 *const)value);

  return new_value;
}

CW_BOOL
cw_data_func_u64_delete (CW_VOID *value)
{
  CW_ERROR_CHECK (value != NULL, CW_BOOL_FALSE,
                  "failed to delete CW_U64, value argument must not be NULL");

  CW_ERROR_CHECK (cw_memory_free (&value) != CW_BOOL_FALSE, CW_BOOL_FALSE,
                  "failed to delete CW_U64, freeing of memory failed");

  return CW_BOOL_TRUE;
}

CW_BOOL
cw_data_func_u64_compare (const CW_VOID *const left,
                          const CW_VOID *const right)
{
  CW_ERROR_CHECK (left != NULL, CW_BOOL_FALSE,
                  "failed to compare CW_U64, left argument must not be NULL");

  CW_ERROR_CHECK (right != NULL, CW_BOOL_FALSE,
                  "failed to compare CW_U64, right argument must not be NULL");

  if (*((const CW_U64 *const)left) == *((const CW_U64 *const)right))
    {
      return CW_BOOL_TRUE;
    }

  return CW_BOOL_FALSE;
}

CW_VOID *
cw_data_func_size_copy (const CW_VOID *const value)
{
  CW_SIZE *new_value = NULL;

  CW_ERROR_CHECK (value != NULL, NULL,
                  "failed to copy CW_SIZE, value argument must not be NULL");

  new_value = cw_memory_allocate (sizeof (CW_SIZE));

  CW_ERROR_CHECK (new_value != NULL, NULL,
                  "failed to copy CW_SIZE, memory allocation failed");

  *new_value = *((const CW_SIZE *const)value);

  return new_value;
}

CW_BOOL
cw_data_func_size_delete (CW_VOID *value)
{
  CW_ERROR_CHECK (value != NULL, CW_BOOL_FALSE,
                  "failed to delete CW_SIZE, value argument must not be NULL");

  CW_ERROR_CHECK (cw_memory_free (&value) != CW_BOOL_FALSE, CW_BOOL_FALSE,
                  "failed to delete CW_SIZE, freeing of memory failed");

  return CW_BOOL_TRUE;
}

CW_BOOL
cw_data_func_size_compare (const CW_VOID *const left,
                           const CW_VOID *const right)
{
  CW_ERROR_CHECK (left != NULL, CW_BOOL_FALSE,
                  "failed to compare CW_SIZE, left argument must not be NULL");

  CW_ERROR_CHECK (
      right != NULL, CW_BOOL_FALSE,
      "failed to compare CW_SIZE, right argument must not be NULL");

  if (*((const CW_SIZE *const)left) == *((const CW_SIZE *const)right))
    {
      return CW_BOOL_TRUE;
    }

  return CW_BOOL_FALSE;
}

CW_VOID *
cw_data_func_bool_copy (const CW_VOID *const value)
{
  CW_BOOL *new_value = NULL;

  CW_ERROR_CHECK (value != NULL, NULL,
                  "failed to copy CW_BOOL, value argument must not be NULL");

  new_value = cw_memory_allocate (sizeof (CW_BOOL));

  CW_ERROR_CHECK (new_value != NULL, NULL,
                  "failed to copy CW_BOOL, memory allocation failed");

  *new_value = *((const CW_BOOL *const)value);

  return new_value;
}

CW_BOOL
cw_data_func_bool_delete (CW_VOID *value)
{
  CW_ERROR_CHECK (value != NULL, CW_BOOL_FALSE,
                  "failed to delete CW_BOOL, value argument must not be NULL");

  CW_ERROR_CHECK (cw_memory_free (&value) != CW_BOOL_FALSE, CW_BOOL_FALSE,
                  "failed to delete CW_BOOL, freeing of memory failed");

  return CW_BOOL_TRUE;
}

CW_BOOL
cw_data_func_bool_compare (const CW_VOID *const left,
                           const CW_VOID *const right)
{
  CW_ERROR_CHECK (left != NULL, CW_BOOL_FALSE,
                  "failed to compare CW_BOOL, left argument must not be NULL");

  CW_ERROR_CHECK (
      right != NULL, CW_BOOL_FALSE,
      "failed to compare CW_BOOL, right argument must not be NULL");

  if (*((const CW_BOOL *const)left) == *((const CW_BOOL *const)right))
    {
      return CW_BOOL_TRUE;
    }

  return CW_BOOL_FALSE;
}

CW_VOID *
cw_data_func_string_copy (const CW_VOID *const value)
{
  CW_CHAR *new_value = NULL;

  CW_ERROR_CHECK (value != NULL, NULL,
                  "failed to copy string, value argument must not be NULL");

  new_value = cw_memory_allocate (
      sizeof (CW_CHAR) * (strlen ((const CW_CHAR *const)value) + 1));

  CW_ERROR_CHECK (new_value != NULL, NULL,
                  "failed to copy string, memory allocation failed");

  strcpy ((CW_CHAR *)new_value, (const CW_CHAR *const)value);

  return new_value;
}

CW_BOOL
cw_data_func_string_delete (CW_VOID *value)
{
  CW_ERROR_CHECK (value != NULL, CW_BOOL_FALSE,
                  "failed to delete string, value argument must not be NULL");

  CW_ERROR_CHECK (cw_memory_free (&value) != CW_BOOL_FALSE, CW_BOOL_FALSE,
                  "failed to delete string, freeing of memory failed");

  return CW_BOOL_TRUE;
}

CW_BOOL
cw_data_func_string_compare (const CW_VOID *const left,
                             const CW_VOID *const right)
{
  CW_ERROR_CHECK (left != NULL, CW_BOOL_FALSE,
                  "failed to compare string, left argument must not be NULL");

  CW_ERROR_CHECK (right != NULL, CW_BOOL_FALSE,
                  "failed to compare string, right argument must not be NULL");

  if (strcmp ((const CW_CHAR *const)left, (const CW_CHAR *const)right) == 0)
    {
      return CW_BOOL_TRUE;
    }

  return CW_BOOL_FALSE;
}
