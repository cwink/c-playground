#include "../inc/cw_data.h"
#include "../inc/cw_error.h"
#include "../inc/cw_memory.h"

#define CW_DATA_SET_LOAD_FACTOR 0.75

#define CW_DATA_SET_DEFAULT_PAIRS_SIZE 16

typedef struct cw_data_set_pair_t
{
  void *value;

  struct cw_data_set_pair_t *next;
} CW_DATA_SET_PAIR;

struct cw_data_set_t
{
  CW_DATA_COPY copy;

  CW_DATA_COMPARE compare;

  CW_DATA_DELETE delete;

  CW_DATA_HASHER hasher;

  CW_DATA_SET_PAIR **pairs;

  CW_SIZE pairs_count;

  CW_SIZE pairs_capacity;
};

static CW_DATA_SET_PAIR *
cw_data_set_pair_create (CW_VOID *const value)
{
  CW_DATA_SET_PAIR *pair = NULL;

  CW_ERROR_CHECK (
      value != NULL, NULL,
      "failed to create set pair, value argument must not be NULL");

  pair = cw_memory_allocate (sizeof (CW_DATA_SET_PAIR));

  CW_ERROR_CHECK (
      pair != NULL, NULL,
      "failed to create set pair, memory allocation of pair failed");

  pair->value = value;

  pair->next = NULL;

  return pair;
}

static CW_DATA_SET_PAIR *
cw_data_set_pair_copy (const CW_DATA_COPY copy,
                       const CW_DATA_SET_PAIR *const pair)
{
  CW_VOID *value = NULL;

  CW_ERROR_CHECK (
      copy != NULL, NULL,
      "failed to copy set pair, copy function argument must not be NULL");

  CW_ERROR_CHECK (pair != NULL, NULL,
                  "failed to copy set pair, pair argument must not be NULL");

  value = copy (pair->value);

  return cw_data_set_pair_create (value);
}

static CW_BOOL
cw_data_set_pair_delete (const CW_DATA_DELETE delete, CW_DATA_SET_PAIR *pair)
{
  CW_ERROR_CHECK (
      delete != NULL, CW_BOOL_FALSE,
      "failed to delete set pair, delete function argument must not be NULL");

  CW_ERROR_CHECK (pair != NULL, CW_BOOL_FALSE,
                  "failed to delete set pair, pair argument must not be NULL");

  CW_ERROR_CHECK (delete (pair->value) != CW_BOOL_FALSE, CW_BOOL_FALSE,
                  "failed to delete set pair, deletion of pair value failed");

  CW_ERROR_CHECK (cw_memory_free ((CW_VOID * *const) & pair) != CW_BOOL_FALSE,
                  CW_BOOL_FALSE,
                  "failed to delete set pair, freeing of pair failed");

  return CW_BOOL_TRUE;
}

CW_DATA_SET *
cw_data_set_create (const CW_DATA_COPY copy, const CW_DATA_COMPARE compare,
                    const CW_DATA_DELETE delete, const CW_DATA_HASHER hasher)
{
  CW_DATA_SET *set = NULL;

  CW_SIZE index = 0;

  CW_ERROR_CHECK (
      copy != NULL, NULL,
      "failed to create set, copy function argument must not be NULL");

  CW_ERROR_CHECK (
      compare != NULL, NULL,
      "failed to create set, compare function argument must not be NULL");

  CW_ERROR_CHECK (
      delete != NULL, NULL,
      "failed to create set, delete function argument must not be NULL");

  CW_ERROR_CHECK (
      hasher != NULL, NULL,
      "failed to create set, hasher function argument must not be NULL");

  set = cw_memory_allocate (sizeof (CW_DATA_SET));

  CW_ERROR_CHECK (set != NULL, NULL,
                  "failed to create set, allocation of set failed");

  set->pairs = cw_memory_allocate (sizeof (CW_DATA_SET_PAIR *)
                                   * CW_DATA_SET_DEFAULT_PAIRS_SIZE);

  CW_ERROR_CHECK (set->pairs != NULL, NULL,
                  "failed to create set, allocation of set pairs failed");

  for (index = 0; index < CW_DATA_SET_DEFAULT_PAIRS_SIZE; ++index)
    {
      set->pairs[index] = NULL;
    }

  set->copy = copy;

  set->compare = compare;

  set->delete = delete;

  set->hasher = hasher;

  set->pairs_capacity = CW_DATA_SET_DEFAULT_PAIRS_SIZE;

  set->pairs_count = 0;

  return set;
}

CW_DATA_SET *
cw_data_set_copy (const CW_DATA_SET *const set)
{
  CW_DATA_SET *new_set = NULL;

  CW_SIZE index = 0;

  CW_ERROR_CHECK (set != NULL, NULL,
                  "failed to copy set, set argument must not be NULL");

  CW_ERROR_CHECK (set->pairs != NULL, NULL,
                  "failed to copy set, set pairs must not be NULL");

  new_set = cw_memory_allocate (sizeof (CW_DATA_SET));

  CW_ERROR_CHECK (new_set != NULL, NULL,
                  "failed to copy set, allocation of new set failed");

  new_set->pairs
      = cw_memory_allocate (sizeof (CW_DATA_SET_PAIR *) * set->pairs_capacity);

  CW_ERROR_CHECK (new_set->pairs != NULL, NULL,
                  "failed to copy set, allocation of set pairs failed");

  for (index = 0; index < set->pairs_capacity; ++index)
    {
      new_set->pairs[index] = NULL;
    }

  for (index = 0; index < set->pairs_capacity; ++index)
    {
      CW_DATA_SET_PAIR *ptr = NULL;

      for (ptr = set->pairs[index]; ptr != NULL; ptr = ptr->next)
        {
          CW_DATA_SET_PAIR *next = NULL;

          if (new_set->pairs[index] == NULL)
            {
              new_set->pairs[index] = cw_data_set_pair_copy (set->copy, ptr);

              CW_ERROR_CHECK (new_set->pairs[index] != NULL, NULL,
                              "failed to copy set, copy of set pair failed");

              continue;
            }

          for (next = new_set->pairs[index]; next->next != NULL;
               next = next->next)
            {
            }

          next->next = cw_data_set_pair_copy (set->copy, ptr);

          CW_ERROR_CHECK (next->next != NULL, NULL,
                          "failed to copy set, copy of set pair failed");
        }
    }

  new_set->copy = set->copy;

  new_set->compare = set->compare;

  new_set->delete = set->delete;

  new_set->hasher = set->hasher;

  new_set->pairs_capacity = set->pairs_capacity;

  new_set->pairs_count = set->pairs_count;

  return new_set;
}

CW_BOOL
cw_data_set_delete (CW_DATA_SET *set)
{
  CW_SIZE index = 0;

  CW_ERROR_CHECK (set != NULL, CW_BOOL_FALSE,
                  "failed to delete set, set argument must not be NULL");

  CW_ERROR_CHECK (set->pairs != NULL, CW_BOOL_FALSE,
                  "failed to delete set, set pairs must not be NULL");

  for (index = 0; index < set->pairs_capacity; ++index)
    {
      CW_DATA_SET_PAIR *ptr = NULL;

      for (ptr = set->pairs[index]; ptr != NULL;)
        {
          CW_DATA_SET_PAIR *next = ptr->next;

          CW_ERROR_CHECK (cw_data_set_pair_delete (set->delete, ptr)
                              != CW_BOOL_FALSE,
                          CW_BOOL_FALSE,
                          "failed to delete set, deletion of set pair failed");

          ptr = next;
        }
    }

  CW_ERROR_CHECK (
      cw_memory_free ((CW_VOID * *const) & set->pairs) != CW_BOOL_FALSE,
      CW_BOOL_FALSE, "failed to delete set, freeing of set pairs failed");

  CW_ERROR_CHECK (cw_memory_free ((CW_VOID * *const) & set) != CW_BOOL_FALSE,
                  CW_BOOL_FALSE,
                  "failed to delete set, freeing of set failed");

  return CW_BOOL_TRUE;
}
