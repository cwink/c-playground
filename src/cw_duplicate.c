#include "../inc/cw_duplicate.h"
#include "../inc/cw_error.h"
#include "../inc/cw_memory.h"

#include <string.h>

CW_CHAR *
cw_duplicate_char (const CW_CHAR value)
{
  CW_CHAR *new_value = cw_memory_allocate (sizeof (CW_CHAR));

  *new_value = value;

  return new_value;
}

CW_S8 *
cw_duplicate_s8 (const CW_S8 value)
{
  CW_S8 *new_value = cw_memory_allocate (sizeof (CW_S8));

  *new_value = value;

  return new_value;
}

CW_U8 *
cw_duplicate_u8 (const CW_U8 value)
{
  CW_U8 *new_value = cw_memory_allocate (sizeof (CW_U8));

  *new_value = value;

  return new_value;
}

CW_S16 *
cw_duplicate_s16 (const CW_S16 value)
{
  CW_S16 *new_value = cw_memory_allocate (sizeof (CW_S16));

  *new_value = value;

  return new_value;
}

CW_U16 *
cw_duplicate_u16 (const CW_U16 value)
{
  CW_U16 *new_value = cw_memory_allocate (sizeof (CW_U16));

  *new_value = value;

  return new_value;
}

CW_S32 *
cw_duplicate_s32 (const CW_S32 value)
{
  CW_S32 *new_value = cw_memory_allocate (sizeof (CW_S32));

  *new_value = value;

  return new_value;
}

CW_U32 *
cw_duplicate_u32 (const CW_U32 value)
{
  CW_U32 *new_value = cw_memory_allocate (sizeof (CW_U32));

  *new_value = value;

  return new_value;
}

CW_S64 *
cw_duplicate_s64 (const CW_S64 value)
{
  CW_S64 *new_value = cw_memory_allocate (sizeof (CW_S64));

  *new_value = value;

  return new_value;
}

CW_U64 *
cw_duplicate_u64 (const CW_U64 value)
{
  CW_U64 *new_value = cw_memory_allocate (sizeof (CW_U64));

  *new_value = value;

  return new_value;
}

CW_SIZE *
cw_duplicate_size (const CW_SIZE value)
{
  CW_SIZE *new_value = cw_memory_allocate (sizeof (CW_SIZE));

  *new_value = value;

  return new_value;
}

CW_BOOL *
cw_duplicate_bool (const CW_BOOL value)
{
  CW_BOOL *new_value = cw_memory_allocate (sizeof (CW_BOOL));

  *new_value = value;

  return new_value;
}

CW_CHAR *
cw_duplicate_string (const CW_CHAR *const string)
{
  CW_CHAR *new_string = NULL;

  CW_ERROR_CHECK (
      string != NULL, NULL,
      "failed to duplicate string, string argument must not be NULL");

  new_string = cw_memory_allocate (sizeof (CW_CHAR) * strlen (string) + 1);

  CW_ERROR_CHECK (
      new_string != NULL, NULL,
      "failed to duplicate string, failed to allocate new string data");

  strcpy (new_string, string);

  return new_string;
}
