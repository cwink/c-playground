#include "../inc/cw_data.h"
#include "../inc/cw_error.h"
#include "../inc/cw_memory.h"

#include <assert.h>

struct cw_data_vector_t
{
  CW_VOID **data;

  CW_DATA_COPY copy;

  CW_DATA_COMPARE compare;

  CW_DATA_DELETE delete;

  CW_SIZE capacity;

  CW_SIZE length;
};

CW_DATA_VECTOR *
cw_data_vector_create (const CW_DATA_COPY copy, const CW_DATA_COMPARE compare,
                       const CW_DATA_DELETE delete, const CW_SIZE capacity)
{
  CW_DATA_VECTOR *vector = NULL;

  CW_SIZE index = 0;

  CW_ERROR_CHECK (
      copy != NULL, NULL,
      "failed to create vector, copy function argument must not be NULL");

  CW_ERROR_CHECK (
      compare != NULL, NULL,
      "failed to create vector, compare function argument must not be NULL");

  CW_ERROR_CHECK (
      delete != NULL, NULL,
      "failed to create vector, delete function argument must not be NULL");

  CW_ERROR_CHECK (
      capacity > 0, NULL,
      "failed to create vector, capacity argument must be greater than zero");

  CW_ERROR_CHECK (
      capacity % 2 == 0, NULL,
      "failed to create vector, capacity argument must be divisible by two");

  vector = cw_memory_allocate (sizeof (CW_DATA_VECTOR));

  CW_ERROR_CHECK (vector != NULL, NULL,
                  "failed to create vector, allocation of vector failed");

  vector->data = cw_memory_allocate (sizeof (CW_VOID *) * capacity);

  CW_ERROR_CHECK (vector->data != NULL, NULL,
                  "failed to create vector, allocation of vector data failed");

  for (index = 0; index < capacity; ++index)
    {
      vector->data[index] = NULL;
    }

  vector->copy = copy;

  vector->compare = compare;

  vector->delete = delete;

  vector->capacity = capacity;

  vector->length = 0;

  return vector;
}

CW_DATA_VECTOR *
cw_data_vector_copy (const CW_DATA_VECTOR *const vector)
{
  CW_DATA_VECTOR *new_vector = NULL;

  CW_SIZE index = 0;

  CW_ERROR_CHECK (vector != NULL, NULL,
                  "failed to copy vector, vector argument must not be NULL");

  new_vector = cw_data_vector_create (vector->copy, vector->compare,
                                      vector->delete, vector->capacity);

  CW_ERROR_CHECK (new_vector != NULL, NULL,
                  "failed to copy vector, creation of a new vector failed");

  new_vector->length = vector->length;

  for (index = 0; index < new_vector->capacity; ++index)
    {
      if (vector->data[index] != NULL)
        {
          new_vector->data[index] = vector->copy (vector->data[index]);

          CW_ERROR_CHECK (new_vector->data[index] != NULL, NULL,
                          "failed to copy vector, copy of data failed");

          continue;
        }

      new_vector->data[index] = NULL;
    }

  return new_vector;
}

CW_BOOL
cw_data_vector_delete (CW_DATA_VECTOR *vector)
{
  CW_SIZE index = 0;

  CW_ERROR_CHECK (vector != NULL, CW_BOOL_FALSE,
                  "failed to delete vector, vector argument must not be NULL");

  for (index = 0; index < vector->capacity; ++index)
    {
      if (vector->data[index] != NULL)
        {
          CW_ERROR_CHECK_1 (vector->delete (vector->data[index])
                                != CW_BOOL_FALSE,
                            CW_BOOL_FALSE,
                            "failed to delete vector, deletion of item in "
                            "vector at index '%d' failed",
                            index);
        }
    }

  CW_ERROR_CHECK (cw_memory_free ((CW_VOID * *const) & vector->data)
                      != CW_BOOL_FALSE,
                  CW_BOOL_FALSE,
                  "failed to delete vector, deletion of vector data failed");

  CW_ERROR_CHECK (
      cw_memory_free ((CW_VOID * *const) & vector) != CW_BOOL_FALSE,
      CW_BOOL_FALSE, "failed to delete vector, deletion of vector failed");

  return CW_BOOL_TRUE;
}

static CW_BOOL
cw_data_vector_resize (CW_DATA_VECTOR *const vector)
{
  CW_VOID **data = NULL;

  CW_SIZE index = 0;

  assert (vector != NULL);

  assert (vector->data != NULL);

  if (vector->length + 2 < vector->capacity)
    {
      return CW_BOOL_TRUE;
    }

  data = cw_memory_reallocate (
      vector->data, sizeof (CW_VOID *) * vector->capacity * vector->capacity);

  CW_ERROR_CHECK (
      data != NULL, CW_BOOL_FALSE,
      "failed to resize vector, reallocation of vector data failed");

  vector->data = data;

  for (index = vector->capacity; index < vector->capacity * vector->capacity;
       ++index)
    {
      vector->data[index] = NULL;
    }

  vector->capacity *= vector->capacity;

  return CW_BOOL_TRUE;
}

CW_BOOL
cw_data_vector_append (CW_DATA_VECTOR *const vector, CW_VOID *const value)
{
  CW_ERROR_CHECK (
      vector != NULL, CW_BOOL_FALSE,
      "failed to append to vector, vector argument must not be NULL");

  CW_ERROR_CHECK (
      value != NULL, CW_BOOL_FALSE,
      "failed to append to vector, value argument must not be NULL");

  vector->data[vector->length] = value;

  ++vector->length;

  return cw_data_vector_resize (vector);
}

CW_BOOL
cw_data_vector_prepend (CW_DATA_VECTOR *const vector, CW_VOID *const value)
{
  CW_SIZE index = 0;

  CW_VOID **data = NULL;

  CW_ERROR_CHECK (
      vector != NULL, CW_BOOL_FALSE,
      "failed to prepend to vector, vector argument must not be NULL");

  CW_ERROR_CHECK (
      value != NULL, CW_BOOL_FALSE,
      "failed to prepend to vector, value argument must not be NULL");

  data = cw_memory_allocate (sizeof (CW_VOID *) * vector->capacity);

  CW_ERROR_CHECK (
      data != NULL, CW_BOOL_FALSE,
      "failed to prepend to vector, allocation of new vector data failed");

  data[0] = value;

  for (index = 0; index < vector->capacity - 1; ++index)
    {
      data[index + 1] = vector->data[index];
    }

  CW_ERROR_CHECK (cw_memory_free ((CW_VOID * *const) & vector->data)
                      != CW_BOOL_FALSE,
                  CW_BOOL_FALSE,
                  "failed to prepend to vector, failed to free vector data");

  vector->data = data;

  ++vector->length;

  return cw_data_vector_resize (vector);
}

CW_BOOL
cw_data_vector_insert (CW_DATA_VECTOR *const vector, const CW_SIZE index,
                       CW_VOID *const value)
{
  CW_VOID **data = NULL;

  CW_SIZE i = 0;

  CW_ERROR_CHECK (
      vector != NULL, CW_BOOL_FALSE,
      "failed to insert into vector, vector argument must not be NULL");

  CW_ERROR_CHECK (
      value != NULL, CW_BOOL_FALSE,
      "failed to insert into vector, value argument must not be NULL");

  CW_ERROR_CHECK (index <= vector->length, CW_BOOL_FALSE,
                  "failed to insert into vector, index is out of bounds");

  if (index == vector->length)
    {
      return cw_data_vector_append (vector, value);
    }

  data = cw_memory_allocate (sizeof (CW_VOID *) * vector->capacity);

  CW_ERROR_CHECK (data != NULL, CW_BOOL_FALSE,
                  "failed to insert into vector, allocation of new data for "
                  "vector failed");

  for (i = 0; i < index; ++i)
    {
      data[i] = vector->data[i];
    }

  data[i] = value;

  for (; i < vector->capacity - 1; ++i)
    {
      data[i + 1] = vector->data[i];
    }

  CW_ERROR_CHECK (cw_memory_free ((CW_VOID * *const) & vector->data)
                      != CW_BOOL_FALSE,
                  CW_BOOL_FALSE,
                  "failed to insert into vector, failed to free vector data");

  vector->data = data;

  ++vector->length;

  return cw_data_vector_resize (vector);
}

CW_VOID *
cw_data_vector_pop_back (CW_DATA_VECTOR *const vector)
{
  CW_VOID *value = NULL;

  CW_ERROR_CHECK (
      vector != NULL, NULL,
      "failed to pop back of vector, vector argument must not be NULL");

  CW_ERROR_CHECK (vector->length != 0, NULL,
                  "failed to pop back of vector, vector is empty");

  value = vector->data[vector->length - 1];

  vector->data[vector->length - 1] = NULL;

  --vector->length;

  return value;
}

CW_VOID *
cw_data_vector_pop_front (CW_DATA_VECTOR *const vector)
{
  CW_SIZE index = 0;

  CW_VOID *value = NULL;

  CW_ERROR_CHECK (
      vector != NULL, NULL,
      "failed to pop front of vector, vector argument must not be NULL");

  CW_ERROR_CHECK (vector->length != 0, NULL,
                  "failed to pop front of vector, vector is empty");

  value = vector->data[0];

  for (index = 1; index < vector->capacity; ++index)
    {
      vector->data[index - 1] = vector->data[index];
    }

  --vector->length;

  return value;
}

CW_BOOL
cw_data_vector_remove_back (CW_DATA_VECTOR *const vector)
{
  CW_VOID *value = NULL;

  CW_ERROR_CHECK (
      vector != NULL, CW_BOOL_FALSE,
      "failed to remove back from vector, vector argument must not be NULL");

  value = cw_data_vector_pop_back (vector);

  CW_ERROR_CHECK (value != NULL, CW_BOOL_FALSE,
                  "failed to remove back from vector, value is empty");

  CW_ERROR_CHECK (
      vector->delete (value) != CW_BOOL_FALSE, CW_BOOL_FALSE,
      "failed to remove back from vector, deletion of value failed");

  return CW_BOOL_TRUE;
}

CW_BOOL
cw_data_vector_remove_front (CW_DATA_VECTOR *const vector)
{
  CW_VOID *value = NULL;

  CW_ERROR_CHECK (
      vector != NULL, CW_BOOL_FALSE,
      "failed to remove front from vector, vector argument must not be NULL");

  value = cw_data_vector_pop_front (vector);

  CW_ERROR_CHECK (value != NULL, CW_BOOL_FALSE,
                  "failed to remove front from vector, value is empty");

  CW_ERROR_CHECK (
      vector->delete (value) != CW_BOOL_FALSE, CW_BOOL_FALSE,
      "failed to remove front from vector, deletion of value failed");

  return CW_BOOL_TRUE;
}

CW_VOID *
cw_data_vector_pop (CW_DATA_VECTOR *const vector, const CW_SIZE index)
{
  CW_VOID *value = NULL;

  CW_SIZE i = 0;

  CW_ERROR_CHECK (
      vector != NULL, NULL,
      "failed to pop from vector, vector argument must not be NULL");

  CW_ERROR_CHECK (index < vector->length, NULL,
                  "failed to pop from vector, index is out of bounds");

  value = vector->data[index];

  for (i = index + 1; i < vector->capacity; ++i)
    {
      vector->data[i - 1] = vector->data[i];
    }

  --vector->length;

  return value;
}

CW_BOOL
cw_data_vector_remove (CW_DATA_VECTOR *const vector, const CW_SIZE index)
{
  CW_VOID *value = NULL;

  CW_ERROR_CHECK (
      vector != NULL, CW_BOOL_FALSE,
      "failed to remove from vector, vector argument must not be NULL");

  value = cw_data_vector_pop (vector, index);

  CW_ERROR_CHECK (value != NULL, CW_BOOL_FALSE,
                  "failed to remove from vector, value is empty");

  CW_ERROR_CHECK_1 (vector->delete (value) != CW_BOOL_FALSE, CW_BOOL_FALSE,
                    "failed to remove item from vector at index '%d'", index);

  return CW_BOOL_TRUE;
}

CW_BOOL
cw_data_vector_clear (CW_DATA_VECTOR *const vector)
{
  CW_SIZE index = 0;

  CW_ERROR_CHECK (vector != NULL, CW_BOOL_FALSE,
                  "failed to clear vector, vector argument must not be NULL");

  for (index = 0; index < vector->capacity; ++index)
    {
      if (vector->data[index] != NULL)
        {
          CW_ERROR_CHECK_1 (vector->delete (vector->data[index])
                                != CW_BOOL_FALSE,
                            CW_BOOL_FALSE,
                            "failed to clear vector, removal of item from "
                            "vector at index '%d' failed",
                            index);

          vector->data[index] = NULL;
        }
    }

  vector->length = 0;

  return CW_BOOL_TRUE;
}

CW_VOID *
cw_data_vector_get (CW_DATA_VECTOR *const vector, const CW_SIZE index)
{
  CW_ERROR_CHECK (
      vector != NULL, NULL,
      "failed to get item from vector, vector argument must not be NULL");

  CW_ERROR_CHECK (vector->length != 0 && index < vector->length, NULL,
                  "failed to get item from vector, index out of bounds");

  return vector->data[index];
}

CW_BOOL
cw_data_vector_set (CW_DATA_VECTOR *const vector, const CW_SIZE index,
                    CW_VOID *const value)
{
  CW_ERROR_CHECK (
      vector != NULL, CW_BOOL_FALSE,
      "failed to set item in vector, vector argument must not be NULL");

  CW_ERROR_CHECK (
      value != NULL, CW_BOOL_FALSE,
      "failed to set item in vector, value argument must not be NULL");

  CW_ERROR_CHECK (vector->length != 0 && index < vector->length, CW_BOOL_FALSE,
                  "failed to set item in vector, index out of bounds");

  CW_ERROR_CHECK (cw_memory_free ((CW_VOID * *const) & vector->data[index])
                      != CW_BOOL_FALSE,
                  CW_BOOL_FALSE,
                  "failed to set item in vector, failed to free old value");

  vector->data[index] = value;

  return CW_BOOL_TRUE;
}

CW_BOOL
cw_data_vector_size (const CW_DATA_VECTOR *const vector, CW_SIZE *size)
{
  CW_ERROR_CHECK (
      vector != NULL, CW_BOOL_FALSE,
      "failed to get vector size, vector argument must not be NULL");

  CW_ERROR_CHECK (size != NULL, CW_BOOL_FALSE,
                  "failed to get vector size, size argument must not be NULL");

  *size = vector->length;

  return CW_BOOL_TRUE;
}

CW_BOOL
cw_data_vector_foreach (CW_DATA_VECTOR *const vector,
                        const CW_DATA_FOREACH foreach)
{
  CW_SIZE index = 0;

  CW_ERROR_CHECK (
      vector != NULL, CW_BOOL_FALSE,
      "failed to loop through vector, vector argument must not be NULL");

  CW_ERROR_CHECK (foreach != NULL, CW_BOOL_FALSE,
                  "failed to loop through vector, foreach function argument "
                  "must not be NULL");

  for (index = 0; index < vector->length; ++index)
    {
      CW_BOOL result = foreach (vector->data[index]);

      if (result != CW_BOOL_TRUE)
        {
          return CW_BOOL_FALSE;
        }
    }

  return CW_BOOL_TRUE;
}
