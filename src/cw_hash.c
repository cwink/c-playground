#include "../inc/cw_hash.h"
#include "../inc/cw_error.h"

#include <assert.h>
#include <string.h>

#define CW_HASH_FNV_OFFSET 14695981039346656037UL

#define CW_HASH_FNV_PRIME 1099511628211UL

CW_BOOL
cw_hash_fnv1 (CW_U64 *const hash, const CW_U8 *const data,
              const CW_SIZE length)
{
  CW_SIZE index = 0;

  CW_ERROR_CHECK (
      hash != NULL, CW_BOOL_FALSE,
      "failed to generate fnv1 hash, hash argument must not be NULL");

  CW_ERROR_CHECK (
      data != NULL, CW_BOOL_FALSE,
      "failed to generate fnv1 hash, data argument must not be NULL");

  *hash = CW_HASH_FNV_OFFSET;

  for (index = 0; index < length; ++index)
    {
      *hash *= CW_HASH_FNV_PRIME;

      *hash ^= (CW_U64)data[index];
    }

  return CW_BOOL_TRUE;
}

CW_BOOL
cw_hash_fnv1_string (CW_U64 *const hash, const CW_VOID *const data)
{
  CW_ERROR_CHECK (
      hash != NULL, CW_BOOL_FALSE,
      "failed to generate fnv1 string hash, hash argument must not be NULL");

  CW_ERROR_CHECK (
      data != NULL, CW_BOOL_FALSE,
      "failed to generate fnv1 string hash, data argument must not be NULL");

  return cw_hash_fnv1 (hash, (const CW_U8 *const)data,
                       strlen ((const char *const)data));
}

CW_BOOL
cw_hash_integer (CW_U64 *const hash, const CW_VOID *const data)
{
  CW_ERROR_CHECK (
      hash != NULL, CW_BOOL_FALSE,
      "failed to generate integer hash, hash argument must not be NULL");

  CW_ERROR_CHECK (
      data != NULL, CW_BOOL_FALSE,
      "failed to generate integer hash, data argument must not be NULL");

  *hash = *((const CW_U64 *const)data);

  return CW_BOOL_TRUE;
}
