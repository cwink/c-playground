#include "../inc/cw_data.h"
#include "../inc/cw_error.h"
#include "../inc/cw_memory.h"

#include <assert.h>

#define CW_DATA_MAP_LOAD_FACTOR 0.75

#define CW_DATA_MAP_DEFAULT_PAIRS_SIZE 16

typedef struct cw_data_map_pair_t
{
  CW_VOID *key;

  CW_VOID *value;

  struct cw_data_map_pair_t *next;
} CW_DATA_MAP_PAIR;

struct cw_data_map_t
{
  CW_DATA_COPY key_copy;

  CW_DATA_COMPARE key_compare;

  CW_DATA_DELETE key_delete;

  CW_DATA_COPY value_copy;

  CW_DATA_DELETE value_delete;

  CW_DATA_HASHER hasher;

  CW_DATA_MAP_PAIR **pairs;

  CW_SIZE pairs_count;

  CW_SIZE pairs_capacity;
};

static CW_DATA_MAP_PAIR *
cw_data_map_pair_create (CW_VOID *const key, CW_VOID *const value)
{
  CW_DATA_MAP_PAIR *pair = NULL;

  assert (key != NULL);

  assert (value != NULL);

  pair = cw_memory_allocate (sizeof (CW_DATA_MAP_PAIR));

  CW_ERROR_CHECK (pair != NULL, NULL,
                  "failed to create map pair, new pair creation failed");

  pair->key = key;

  pair->value = value;

  pair->next = NULL;

  return pair;
}

static CW_DATA_MAP_PAIR *
cw_data_map_pair_copy (const CW_DATA_COPY key_copy,
                       const CW_DATA_DELETE key_delete,
                       const CW_DATA_COPY value_copy,
                       const CW_DATA_MAP_PAIR *const pair)
{
  CW_VOID *key = NULL;

  CW_VOID *value = NULL;

  assert (key_copy != NULL);

  assert (key_delete != NULL);

  assert (value_copy != NULL);

  assert (pair != NULL);

  key = key_copy (pair->key);

  CW_ERROR_CHECK (key != NULL, NULL, "failed to copy pair, copy key failed");

  value = value_copy (pair->value);

  CW_ERROR_CHECK (value != NULL, NULL,
                  "failed to copy pair, copy value failed");

  return cw_data_map_pair_create (key, value);
}

static CW_BOOL
cw_data_map_pair_delete (const CW_DATA_DELETE key_delete,
                         const CW_DATA_DELETE value_delete,
                         CW_DATA_MAP_PAIR *pair)
{
  assert (key_delete != NULL);

  assert (value_delete != NULL);

  assert (pair != NULL);

  assert (pair->key != NULL);

  CW_ERROR_CHECK (key_delete (pair->key) != CW_BOOL_FALSE, CW_BOOL_FALSE,
                  "failed to delete pair, key deletion failed");

  CW_ERROR_CHECK (value_delete (pair->value) != CW_BOOL_FALSE, CW_BOOL_FALSE,
                  "failed to delete pair, value deletion failed");

  CW_ERROR_CHECK (cw_memory_free ((CW_VOID * *const) & pair) != CW_BOOL_FALSE,
                  CW_BOOL_FALSE,
                  "failed to delete pair, freeing of pair failed");

  return CW_BOOL_TRUE;
}

static CW_BOOL
cw_data_map_resize (CW_DATA_MAP *const map)
{
  CW_SIZE index = 0;

  CW_DATA_MAP_PAIR **pairs = NULL;

  if (((CW_R64)map->pairs_count / (CW_R64)map->pairs_capacity)
      < CW_DATA_MAP_LOAD_FACTOR)
    {
      return CW_BOOL_TRUE;
    }

  pairs = cw_memory_allocate (sizeof (CW_DATA_MAP_PAIR *)
                              * (map->pairs_capacity * 2));

  CW_ERROR_CHECK (
      pairs != NULL, CW_BOOL_FALSE,
      "failed to resize map pairs, unable to allocate new pairs memory");

  for (index = 0; index < map->pairs_capacity * 2; ++index)
    {
      pairs[index] = NULL;
    }

  for (index = 0; index < map->pairs_capacity; ++index)
    {
      CW_DATA_MAP_PAIR *ptr = NULL;

      for (ptr = map->pairs[index]; ptr != NULL;)
        {
          CW_U64 hash = 0UL;

          CW_DATA_MAP_PAIR *next = NULL;

          CW_DATA_MAP_PAIR *ptr_next = ptr->next;

          CW_ERROR_CHECK (
              map->hasher (&hash, ptr->key) != CW_BOOL_FALSE, CW_BOOL_FALSE,
              "failed to resize map pairs, generation of hash value failed");

          hash %= map->pairs_capacity * 2;

          if (pairs[hash] == NULL)
            {
              pairs[hash] = ptr;

              pairs[hash]->next = NULL;

              ptr = ptr_next;

              continue;
            }

          for (next = pairs[hash]; next->next != NULL; next = next->next)
            {
            }

          next->next = ptr;

          next->next->next = NULL;

          ptr = ptr_next;
        }
    }

  CW_ERROR_CHECK (
      cw_memory_free ((CW_VOID * *const) & (map->pairs)) != CW_BOOL_FALSE,
      CW_BOOL_FALSE,
      "failed to resize map pairs, freeing of original pairs failed");

  map->pairs = pairs;

  map->pairs_capacity *= 2;

  return CW_BOOL_TRUE;
}

CW_DATA_MAP *
cw_data_map_create (const CW_DATA_COPY key_copy,
                    const CW_DATA_COMPARE key_compare,
                    const CW_DATA_DELETE key_delete,
                    const CW_DATA_COPY value_copy,
                    const CW_DATA_DELETE value_delete,
                    const CW_DATA_HASHER hasher)
{
  CW_SIZE index = 0;

  CW_DATA_MAP *map = NULL;

  CW_ERROR_CHECK (
      key_copy != NULL, NULL,
      "failed to create map, key_copy function argument must not be NULL");

  CW_ERROR_CHECK (
      key_compare != NULL, NULL,
      "failed to create map, key_compare function argument must not be NULL");

  CW_ERROR_CHECK (
      key_delete != NULL, NULL,
      "failed to create map, key_delete function argument must not be NULL");

  CW_ERROR_CHECK (
      value_copy != NULL, NULL,
      "failed to create map, value_copy function argument must not be NULL");

  CW_ERROR_CHECK (
      value_delete != NULL, NULL,
      "failed to create map, value_delete function argument must not be NULL");

  CW_ERROR_CHECK (
      hasher != NULL, NULL,
      "failed to create map, hasher function argument must not be NULL");

  map = cw_memory_allocate (sizeof (CW_DATA_MAP));

  CW_ERROR_CHECK (map != NULL, NULL,
                  "failed to create map, allocation of new map failed");

  map->pairs = cw_memory_allocate (sizeof (CW_DATA_MAP_PAIR *)
                                   * CW_DATA_MAP_DEFAULT_PAIRS_SIZE);

  CW_ERROR_CHECK (map->pairs != NULL, NULL,
                  "failed to create map, allocation of new map pairs failed");

  for (index = 0; index < CW_DATA_MAP_DEFAULT_PAIRS_SIZE; ++index)
    {
      map->pairs[index] = NULL;
    }

  map->key_copy = key_copy;

  map->key_compare = key_compare;

  map->key_delete = key_delete;

  map->value_copy = value_copy;

  map->value_delete = value_delete;

  map->hasher = hasher;

  map->pairs_count = 0;

  map->pairs_capacity = CW_DATA_MAP_DEFAULT_PAIRS_SIZE;

  return map;
}

CW_DATA_MAP *
cw_data_map_copy (const CW_DATA_MAP *const map)
{
  CW_SIZE index = 0;

  CW_DATA_MAP *new_map = NULL;

  CW_ERROR_CHECK (map != NULL, NULL,
                  "failed to copy map, map argument must not be NULL");

  CW_ERROR_CHECK (map->pairs != NULL, NULL,
                  "failed to copy map, map pairs must not be NULL");

  new_map = cw_memory_allocate (sizeof (CW_DATA_MAP));

  CW_ERROR_CHECK (new_map != NULL, NULL,
                  "failed to copy map, allocation of new map failed");

  new_map->pairs
      = cw_memory_allocate (sizeof (CW_DATA_MAP_PAIR *) * map->pairs_capacity);

  CW_ERROR_CHECK (new_map->pairs != NULL, NULL,
                  "failed to copy map, allocation of new map pairs failed");

  for (index = 0; index < map->pairs_capacity; ++index)
    {
      new_map->pairs[index] = NULL;
    }

  for (index = 0; index < map->pairs_capacity; ++index)
    {
      CW_DATA_MAP_PAIR *next = NULL;

      for (next = map->pairs[index]; next != NULL; next = next->next)
        {
          CW_DATA_MAP_PAIR *new_next = NULL;

          if (new_map->pairs[index] == NULL)
            {
              new_map->pairs[index] = cw_data_map_pair_copy (
                  map->key_copy, map->key_delete, map->value_copy, next);

              CW_ERROR_CHECK (
                  new_map->pairs[index] != NULL, NULL,
                  "failed to copy map, copy of new map pair failed");

              continue;
            }

          for (new_next = new_map->pairs[index]; new_next->next != NULL;
               new_next = new_next->next)
            {
            }

          new_next->next = cw_data_map_pair_copy (
              map->key_copy, map->key_delete, map->value_copy, next);

          CW_ERROR_CHECK (new_next->next != NULL, NULL,
                          "failed to copy map, copy of new map pair failed");
        }
    }

  new_map->key_copy = map->key_copy;

  new_map->key_compare = map->key_compare;

  new_map->key_delete = map->key_delete;

  new_map->value_copy = map->value_copy;

  new_map->value_delete = map->value_delete;

  new_map->hasher = map->hasher;

  new_map->pairs_count = map->pairs_count;

  new_map->pairs_capacity = map->pairs_capacity;

  return new_map;
}

CW_BOOL
cw_data_map_delete (CW_DATA_MAP *map)
{
  CW_SIZE index = 0;

  CW_ERROR_CHECK (map != NULL, CW_BOOL_FALSE,
                  "failed to delete map, map argument must not be NULL");

  CW_ERROR_CHECK (map->pairs != NULL, CW_BOOL_FALSE,
                  "failed to delete map, map pairs must not be NULL");

  for (index = 0; index < map->pairs_capacity; ++index)
    {
      CW_DATA_MAP_PAIR *next = NULL;

      for (next = map->pairs[index]; next != NULL;)
        {
          CW_DATA_MAP_PAIR *new_next = next->next;

          CW_ERROR_CHECK (cw_data_map_pair_delete (map->key_delete,
                                                   map->value_delete, next)
                              != CW_BOOL_FALSE,
                          CW_BOOL_FALSE,
                          "failed to delete map, deletion of map pair failed");

          next = new_next;
        }
    }

  CW_ERROR_CHECK (
      cw_memory_free ((CW_VOID * *const) & (map->pairs)) != CW_BOOL_FALSE,
      CW_BOOL_FALSE, "failed to delete map, freeing of pairs failed");

  CW_ERROR_CHECK (cw_memory_free ((CW_VOID * *const) & map) != CW_BOOL_FALSE,
                  CW_BOOL_FALSE,
                  "failed to delete map, freeing of map failed");

  return CW_BOOL_TRUE;
}

CW_BOOL
cw_data_map_set (CW_DATA_MAP *const map, CW_VOID *const key,
                 CW_VOID *const value)
{
  CW_U64 hash = 0UL;

  CW_DATA_MAP_PAIR *ptr = NULL;

  CW_ERROR_CHECK (map != NULL, CW_BOOL_FALSE,
                  "failed to set value in map, map argument must not be NULL");

  CW_ERROR_CHECK (map->pairs != NULL, CW_BOOL_FALSE,
                  "failed to set value in map, map pairs must not be NULL");

  CW_ERROR_CHECK (key != NULL, CW_BOOL_FALSE,
                  "failed to set value in map, key argument must not be NULL");

  CW_ERROR_CHECK (
      value != NULL, CW_BOOL_FALSE,
      "failed to set value in map, value argument must not be NULL");

  CW_ERROR_CHECK (
      map->hasher (&hash, key) != CW_BOOL_FALSE, CW_BOOL_FALSE,
      "failed to set value in map, generation of hash value failed");

  hash %= map->pairs_capacity;

  if (map->pairs[hash] == NULL)
    {
      map->pairs[hash] = cw_data_map_pair_create (key, value);

      CW_ERROR_CHECK (map->pairs[hash] != NULL, CW_BOOL_FALSE,
                      "failed to set value in map, creation of pair failed");

      ++map->pairs_count;

      return cw_data_map_resize (map);
    }

  if (map->key_compare (map->pairs[hash]->key, key) == CW_BOOL_TRUE)
    {
      CW_ERROR_CHECK (
          map->key_delete (key) != CW_BOOL_FALSE, CW_BOOL_FALSE,
          "failed to set value in map, could not delete key argument");

      CW_ERROR_CHECK (
          map->value_delete (map->pairs[hash]->value) != CW_BOOL_FALSE,
          CW_BOOL_FALSE,
          "failed to set value in map, could not delete original value");

      map->pairs[hash]->value = value;

      ++map->pairs_count;

      return cw_data_map_resize (map);
    }

  for (ptr = map->pairs[hash]; ptr->next != NULL; ptr = ptr->next)
    {
      if (map->key_compare (ptr->next->key, key) == CW_BOOL_TRUE)
        {
          CW_ERROR_CHECK (
              map->key_delete (key) != CW_BOOL_FALSE, CW_BOOL_FALSE,
              "failed to set value in map, could not delete key argument");

          CW_ERROR_CHECK (
              map->value_delete (ptr->next->value) != CW_BOOL_FALSE,
              CW_BOOL_FALSE,
              "failed to set value in map, could not delete original value");

          ptr->next->value = value;

          ++map->pairs_count;

          return cw_data_map_resize (map);
        }
    }

  ptr->next = cw_data_map_pair_create (key, value);

  CW_ERROR_CHECK (ptr->next != NULL, CW_BOOL_FALSE,
                  "failed to set value in map, could not create next pair");

  ++map->pairs_count;

  return cw_data_map_resize (map);
}

CW_VOID *
cw_data_map_get (CW_DATA_MAP *const map, const CW_VOID *const key)
{
  CW_U64 hash = 0UL;

  CW_DATA_MAP_PAIR *ptr = NULL;

  CW_ERROR_CHECK (map != NULL, NULL,
                  "failed to get value in map, map argument must not be NULL");

  CW_ERROR_CHECK (map->pairs != NULL, NULL,
                  "failed to get value in map, map pairs must not be NULL");

  CW_ERROR_CHECK (map->hasher (&hash, key) != CW_BOOL_FALSE, NULL,
                  "failed to get value in map, could not generate hash value");

  hash %= map->pairs_capacity;

  for (ptr = map->pairs[hash]; ptr != NULL; ptr = ptr->next)
    {
      if (map->key_compare (ptr->key, key) == CW_BOOL_TRUE)
        {
          return ptr->value;
        }
    }

  CW_ERROR_WRITE ("failed to get value in map, could not find key in map");

  return NULL;
}

CW_BOOL
cw_data_map_erase (CW_DATA_MAP *const map, const CW_VOID *const key)
{
  CW_U64 hash = 0UL;

  CW_DATA_MAP_PAIR *ptr = NULL;

  CW_ERROR_CHECK (
      map != NULL, CW_BOOL_FALSE,
      "failed to erase value in map, map argument must not be NULL");

  CW_ERROR_CHECK (map->pairs != NULL, CW_BOOL_FALSE,
                  "failed to erase value in map, map pairs must not be NULL");

  CW_ERROR_CHECK (
      map->hasher (&hash, key) != CW_BOOL_FALSE, CW_BOOL_FALSE,
      "failed to erase value in map, could not generate hash value");

  hash %= map->pairs_capacity;

  CW_ERROR_CHECK (map->pairs[hash] != NULL, CW_BOOL_FALSE,
                  "failed to erase value in map, could not find key in map");

  if (map->key_compare (map->pairs[hash]->key, key) == CW_BOOL_TRUE)
    {
      CW_DATA_MAP_PAIR *next = map->pairs[hash]->next;

      CW_ERROR_CHECK (
          cw_data_map_pair_delete (map->key_delete, map->value_delete,
                                   map->pairs[hash])
              != CW_BOOL_FALSE,
          CW_BOOL_FALSE,
          "failed to erase value in map, deletion of map pair failed");

      map->pairs[hash] = next;

      --map->pairs_count;

      return CW_BOOL_TRUE;
    }

  for (ptr = map->pairs[hash]; ptr->next != NULL; ptr = ptr->next)
    {
      if (map->key_compare (ptr->next->key, key) == CW_BOOL_TRUE)
        {
          CW_DATA_MAP_PAIR *next = ptr->next->next;

          CW_ERROR_CHECK (
              cw_data_map_pair_delete (map->key_delete, map->value_delete,
                                       ptr->next)
                  != CW_BOOL_FALSE,
              CW_BOOL_FALSE,
              "failed to erase value in map, deletion of map pair failed");

          ptr->next = next;

          --map->pairs_count;

          return CW_BOOL_TRUE;
        }
    }

  CW_ERROR_WRITE ("failed to erase value in map, could not find key in map");

  return CW_BOOL_FALSE;
}

CW_BOOL
cw_data_map_has (CW_DATA_MAP *const map, const CW_VOID *const key)
{
  CW_U64 hash = 0UL;

  CW_DATA_MAP_PAIR *ptr = NULL;

  CW_ERROR_CHECK (
      map != NULL, CW_BOOL_FALSE,
      "failed to check if value is in map, map argument must not be NULL");

  CW_ERROR_CHECK (
      map->pairs != NULL, CW_BOOL_FALSE,
      "failed to check if value is in map, map pairs must not be NULL");

  CW_ERROR_CHECK (
      map->hasher (&hash, key) != CW_BOOL_FALSE, CW_BOOL_FALSE,
      "failed to check if value is in map, could not generate hash value");

  hash %= map->pairs_capacity;

  for (ptr = map->pairs[hash]; ptr != NULL; ptr = ptr->next)
    {
      if (map->key_compare (ptr->key, key) == CW_BOOL_TRUE)
        {
          return CW_BOOL_TRUE;
        }
    }

  return CW_BOOL_FALSE;
}

CW_BOOL
cw_data_map_size (const CW_DATA_MAP *const map, CW_SIZE *size)
{
  CW_ERROR_CHECK (map != NULL, CW_BOOL_FALSE,
                  "failed to get map size, map argument must not be NULL");

  CW_ERROR_CHECK (size != NULL, CW_BOOL_FALSE,
                  "failed to get map size, size argument must not be NULL");

  *size = map->pairs_count;

  return CW_BOOL_TRUE;
}

CW_BOOL
cw_data_map_foreach (CW_DATA_MAP *const map,
                     const CW_DATA_FOREACH_PAIR foreach)
{
  CW_SIZE index = 0;

  CW_ERROR_CHECK (map != NULL, CW_BOOL_FALSE,
                  "failed to loop through map, map argument must not be NULL");

  CW_ERROR_CHECK (map->pairs != NULL, CW_BOOL_FALSE,
                  "failed to loop through map, map pairs must not be NULL");

  CW_ERROR_CHECK (foreach != NULL, CW_BOOL_FALSE,
                  "failed to loop through map, foreach function argument must "
                  "not be NULL");

  for (index = 0; index < map->pairs_capacity; ++index)
    {
      CW_DATA_MAP_PAIR *ptr = NULL;

      for (ptr = map->pairs[index]; ptr != NULL; ptr = ptr->next)
        {
          CW_BOOL result = foreach (ptr->key, ptr->value);

          if (result != CW_BOOL_TRUE)
            {
              return CW_BOOL_FALSE;
            }
        }
    }

  return CW_BOOL_TRUE;
}
